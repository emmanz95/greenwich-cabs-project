# Greenwich Cabs App
Greenwich Cabs App is written using Netbeans java swing bootstrapped with spring boot version 2.2.0 framework
Greenwich Cabs App uses embedded derby database and csv for storing and displaying data.

## Notes
**Do not connect to derby database whilst running the application as you can not have more than 1 connection**

## Run Application
* Using netbeans, open the project and click on clean and build. When clean and building the project
it will download the Libraries/Dependency from maven repository that was defined from pom.xml file.

![Scheme](readme-images/clean-and-build-project.png)

* Dependencies that was downloaded defined from pom can be found in your local machine under .m2 folder
~~~
Example path of where m2 folder can be found:
C:\Users\admin\.m2\repository
~~~

* Once the build is successful, just **Run** the project

![Scheme](readme-images/run-app-menu.png)

![Scheme](readme-images/run-app-journeys.png)

## How to connect to derby database
* On Netbeans, use the service tab and you will find Database. Right click on database and click on new connection

![Scheme](readme-images/add-database-connection.png)

* Add the connection driver required for java derby db. You will find the jar file of the dependency in the **.m2 repository folder** under 
**C:\Users\admin\\.m2\repository\org\apache\derby\derby\10.14.2.0\10.14.2.0.jar**

![Scheme](readme-images/add-derby-jar-file-from-local-m2-folder.png)

* Provide connection properties and the location of the database. Specify **create=true** to create the database automatically by default. Click on **Test connection** to see if it works and then click next and then finish.

![Scheme](readme-images/provide-db-connection-properties.png)

* You need to provide the same connection properties hard coded on the application to connect to database

![Scheme](readme-images/database-connection-code.png)

* Create tables and execute queries by right click on the **greenwich_cabs_db** and click on execute command

![Scheme](readme-images/excute-command-query.png)

![Scheme](readme-images/create-tables-and-commands.png)

* Once database is ready and insert data for Driver Table, now run the project

* Look under **src\main\resources\sql** folder which includes sql scripts

## Changing between to use Derby DB, CSV and mock in-memory List

* From JourneyDispatcherServiceForm class, you can change what source of connection you want the application to run with
~~~
    // connect to derby-db 
    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherDerbyServiceImpl();
    // connect to csv
    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherCsvServiceImpl();
    // mock using in memory list
    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherMockServiceImpl();
~~~
![Scheme](readme-images/three-sources-of-connectivity.png)

## Libraries (Dependencies)
Dependencies used in the project is as follows:
~~~
<!-- based on https://github.com/LGoodDatePicker/LGoodDatePicker, LGoodDatePicker is library which is used for date picker on the swing form UI -->
        <dependency>
            <groupId>com.github.lgooddatepicker</groupId>
            <artifactId>LGoodDatePicker</artifactId>
            <version>10.3.1</version>
        </dependency>
<!-- based on https://www.baeldung.com/opencsv, opencsv is a csv library which is used for writing and reading to csv -->
        <dependency>
            <groupId>com.opencsv</groupId>
            <artifactId>opencsv</artifactId>
            <version>5.0</version>
        </dependency>

<!--database dependency for derby database and using jdbc queries-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>
<!--the jar file from derby dependency is used for connecting to the database--> 
        <dependency>
            <groupId>org.apache.derby</groupId>
            <artifactId>derby</artifactId>            
<!--lombok is a library that is used to reduce boiler template code to replace getters and setters-->
<!--slf4j is used for logging on console very similar to system.out.println -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
~~~

