CREATE TABLE Driver (
   driver_id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
   username VARCHAR(255),
   password VARCHAR(255),
   PRIMARY KEY (driver_id),
   UNIQUE(username)
);

CREATE TABLE Journey (
   journey_id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
   start_time TIMESTAMP NOT NULL,
   pickup_point VARCHAR(255) NOT NULL,
   destination VARCHAR(255) NOT NULL,
   driver_id BIGINT NOT NULL,
   journey_status VARCHAR(255) NOT NULL,
   name_of_passenger VARCHAR(255),
   fare FLOAT,
   optional_tip FLOAT,
   account_name VARCHAR(255),
   telephone_no VARCHAR(255),
   end_time TIMESTAMP,
   active BOOLEAN,
   PRIMARY KEY (journey_id)
);
