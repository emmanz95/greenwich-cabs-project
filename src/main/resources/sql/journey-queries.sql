-- INSERT INTO App.Journey(start_time, pickup_point, destination, driver_id, journey_status, active) VALUES(:startTime, :pickupPoint, :destination, :driverId, :journeyStatus, :active)

-- UPDATE App.Journey SET fare=:fare, telephone_no=:telephoneNo, optional_tip=:optionalTip, journey_status=:journeyStatus, name_of_passenger=:nameOfPassenger, end_time=:endTime WHERE journey_id=:journeyId;

-- SELECT {fn TIMESTAMPADD(SQL_TSI_MINUTE, 10, j.END_TIME) } as end_date_time, j.JOURNEY_STATUS, j.JOURNEY_ID FROM App.Journey j where j.DRIVER_ID = 1 and (j.JOURNEY_STATUS = 'COMPLETED' OR j.JOURNEY_STATUS = 'PROGRESS') and 
-- (({fn TIMESTAMPADD(SQL_TSI_MINUTE, 10, j.END_TIME) } is null and j.START_TIME <= '2019-10-28 00:00:00') OR ({fn TIMESTAMPADD(SQL_TSI_MINUTE, 10, j.END_TIME) } is not null and j.START_TIME <= '2019-10-28 00:00:00' and {fn TIMESTAMPADD(SQL_TSI_MINUTE, 10, j.END_TIME) } > '2019-10-28 00:00:00')) 
-- fetch first 1 rows only;

SELECT COALESCE(SUM(j.fare), 0) as avg_fare, COALESCE(SUM(j.optional_tip), 0) as avg_tip, COUNT(*) as total_completed, driver_id  FROM App.Journey j where DATE(j.start_time) = DATE('2019-10-28 00:00:00')
AND j.driver_id=1 AND j.journey_status='COMPLETED';
-- 
SELECT COALESCE(SUM(j.fare), 0) as avg_fare, COALESCE(SUM(j.optional_tip), 0) as avg_tip, COUNT(*) as total_completed  FROM App.Journey j where DATE(j.start_time) = DATE('2019-11-17 00:00:00')
AND j.journey_status='COMPLETED';
