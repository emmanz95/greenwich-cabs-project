package com.gre.cabs.form.table.model;

import com.gre.cabs.dto.JourneyDispatcherDto;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author admin
 */
// Based on https://stackoverflow.com/questions/7378013/connect-a-list-of-objects-to-a-jtable
// created custom table model for Journey Dispatcher details
public class JourneyDispatcherModelTable extends AbstractTableModel {

    // instantiating list of journeys
    private List<JourneyDispatcherDto> journeyList = new ArrayList();
    // this is the date time format that is to be shown on table
    // based on https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
    // had to create the pattern that format needs to be converted to
    private final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    // column names
    private String[] columnNames = {
        "Journey ID", "Journey Status" ,"Time", "Pick Up Point", "Destination", "Driver", "Passenger Name", "Fare", "Optional Tip", "Account Name", "Telephone"
    };

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex]; // get column name based on index
    }

    public JourneyDispatcherModelTable(List<JourneyDispatcherDto> list) {
        this.journeyList = list; // set the list
    }

    @Override
    public int getRowCount() {
        return journeyList.size(); // getting total row count from list
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length; // get column count
    }

    // getting value from each cell
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // getting value from list based on row index
        JourneyDispatcherDto dispatcher = journeyList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                // getting journey id (Long class type)
                return dispatcher.getJourneyId();
            case 1:
                // getting journey status (String class type)
                return dispatcher.getJourneyStatus()!= null
                        ? dispatcher.getJourneyStatus().getStatusType()
                        : null;
            case 2:
                // getting start time (String class type)
                return dispatcher.getStartTime() != null
                        ? format.format(dispatcher.getStartTime())
                        : null;
            case 3:
                // getting pickup point (String class type)
                return dispatcher.getPickUpPoint();
            case 4:
                // getting destination (String class type)
                return dispatcher.getDestination();
            case 5:
                // getting driver id (Long class type)
                return dispatcher.getDriverId();
            case 6:
                // getting name of passenger (String class type)
                return dispatcher.getNameOfPassenger();
            case 7:
                // getting fare (Float class type)
                return dispatcher.getFare();
            case 8:
                // getting tip (Float class type)
                return dispatcher.getOptionalTip();
            case 9:
                // getting name of account (String class type)
                return dispatcher.getAccount();
            case 10:
                // getting telephone number (String class type)
                return dispatcher.getTelephoneNo();
        }
        return null;
    }
    
    public JourneyDispatcherDto getValue(int rowIndex) {
        return journeyList.get(rowIndex); // getting Journey Dispatch object from index
    }

    // getting class type of each column based on getValueAt
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Long.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return Long.class;
            case 6:
                return String.class;
            case 7:
                return Float.class;
            case 8:
                return Float.class;
            case 9:
                return String.class;
            case 10:
                return String.class;
        }
        return null;
    }
}
