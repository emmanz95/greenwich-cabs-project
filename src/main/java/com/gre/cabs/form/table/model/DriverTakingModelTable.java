package com.gre.cabs.form.table.model;

import com.gre.cabs.dto.form.DriverTakingDto;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author admin
 */
// Based on https://stackoverflow.com/questions/7378013/connect-a-list-of-objects-to-a-jtable
// created custom table model for driver taking
public class DriverTakingModelTable extends AbstractTableModel {

    // instantiating list of driver takings
    private List<DriverTakingDto> driverTakingList = new ArrayList();
    // based on https://javarevisited.blogspot.com/2012/03/how-to-format-decimal-number-in-java.html
    // format to currency nearest 2 decimal places
    private final DecimalFormat moneyFormat = new DecimalFormat("£0.00");
    // column names
    private String[] columnNames = {
        "Driver", "Takings" ,"%", "Jobs done"
    };

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex]; // get column name based on index
    }

    public DriverTakingModelTable(List<DriverTakingDto> list) {
        this.driverTakingList = list; // set the list
    }

    @Override
    public int getRowCount() {
        return driverTakingList.size(); // getting total row count from list
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length; // get column count
    }

    // getting value from each cell
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // getting value from list based on row index
        DriverTakingDto driverTaking = driverTakingList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                // getting driver id (Long class type)
                return driverTaking.getDriverId();
            case 1:
                // getting driver takings in string format (String class type)
                return driverTaking.getTakings()!= null
                        ? moneyFormat.format(driverTaking.getTakings())
                        : moneyFormat.format(0.00F);
            case 2:
                // getting driver takings in string format (String class type)
                return driverTaking.getPercentage() != null
                        ? moneyFormat.format(driverTaking.getPercentage())
                        : moneyFormat.format(0.00F);
            case 3:
                // getting jobs done (Long class type)
                return driverTaking.getJobsDone() != null
                        ? driverTaking.getJobsDone()
                        : 0L;
        }
        return null;
    }
    
    public DriverTakingDto getValue(int rowIndex) {
        return driverTakingList.get(rowIndex); // getting Driver taking object from index
    }

    // getting class type of each column based on getValueAt
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Long.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Long.class;
        }
        return null;
    }
}
