package com.gre.cabs.form;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.form.table.model.JourneyDispatcherModelTable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

// JourneyDispatcherForm to manage journeys
// extending to JourneyDispatcherServiceForm which extends to jframe
@Slf4j
public class JourneyDispatcherForm extends JourneyDispatcherServiceForm {

    private List<JourneyDispatcherDto> journeyDispatchList = dispatcherService.getDispatcherDetails();

    // forms required to create new or update journey
    private final NewJourneyForm newJourneyForm = new NewJourneyForm();
    private final UpdateJourneyForm completeJourneyForm = new UpdateJourneyForm();

    /**
     * Creates new form AdminForm
     */
    public JourneyDispatcherForm() {
        this.initComponents();
    }

    // update journey table when adding new journey
    public void addNewJourneyToTable(JourneyDispatcherDto journeyDispatcherDto) {
        this.tblJourneyDispatcher.setModel(new JourneyDispatcherModelTable(journeyDispatchList));
    }
    // update journey table when updating journey
    public void updateJourneyToTable(JourneyDispatcherDto journeyDispatcherDto) {
        this.tblJourneyDispatcher.setModel(new JourneyDispatcherModelTable(journeyDispatchList));
        tblJourneyDispatcher.clearSelection();
        this.btnUpdateJourney.setVisible(false);
        this.btnResetSelection.setVisible(false);
    }
    
    // reset fields
    public void reset() {
        this.journeyDispatchList = dispatcherService.getDispatcherDetails();
        this.tblJourneyDispatcher.setModel(new JourneyDispatcherModelTable(journeyDispatchList));
        tblJourneyDispatcher.clearSelection();
        this.btnUpdateJourney.setVisible(false);
        this.btnResetSelection.setVisible(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblGreenwichCabs = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnNewJourney = new javax.swing.JButton();
        btnUpdateJourney = new javax.swing.JButton();
        pnlJourneyDispatcher = new javax.swing.JPanel();
        spJourneyDispatcher = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblJourneyDispatcher = new javax.swing.JTable();
        btnResetSelection = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(77, 19, 209));

        lblGreenwichCabs.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        lblGreenwichCabs.setForeground(new java.awt.Color(0, 235, 39));
        lblGreenwichCabs.setText("Greenwich Cabs");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblGreenwichCabs)
                .addGap(512, 512, 512))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(lblGreenwichCabs, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(200, 247, 197));

        btnNewJourney.setBackground(new java.awt.Color(136, 238, 231));
        btnNewJourney.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-text-28-24.png"))); // NOI18N
        btnNewJourney.setText("Add New Journey");
        btnNewJourney.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnNewJourney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewJourneyActionPerformed(evt);
            }
        });

        btnUpdateJourney.setBackground(new java.awt.Color(136, 238, 231));
        btnUpdateJourney.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-refresh-7-24.png"))); // NOI18N
        btnUpdateJourney.setText("Update Journey");
        btnUpdateJourney.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.btnUpdateJourney.setVisible(false);
        btnUpdateJourney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateJourneyActionPerformed(evt);
            }
        });

        // added our custom model and setting the initial list of journey details
        tblJourneyDispatcher.setModel(new JourneyDispatcherModelTable(journeyDispatchList));
        tblJourneyDispatcher.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblJourneyDispatcherMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblJourneyDispatcher);

        spJourneyDispatcher.setViewportView(jScrollPane1);

        javax.swing.GroupLayout pnlJourneyDispatcherLayout = new javax.swing.GroupLayout(pnlJourneyDispatcher);
        pnlJourneyDispatcher.setLayout(pnlJourneyDispatcherLayout);
        pnlJourneyDispatcherLayout.setHorizontalGroup(
            pnlJourneyDispatcherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJourneyDispatcherLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spJourneyDispatcher, javax.swing.GroupLayout.PREFERRED_SIZE, 1099, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlJourneyDispatcherLayout.setVerticalGroup(
            pnlJourneyDispatcherLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJourneyDispatcherLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spJourneyDispatcher, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnResetSelection.setBackground(new java.awt.Color(136, 238, 231));
        btnResetSelection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-magnifier-12-24.png"))); // NOI18N
        btnResetSelection.setText("Reset");
        btnResetSelection.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.btnResetSelection.setVisible(false);
        btnResetSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetSelectionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnNewJourney, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnUpdateJourney, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnResetSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(pnlJourneyDispatcher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlJourneyDispatcher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(btnNewJourney, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(btnUpdateJourney, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43)
                        .addComponent(btnResetSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblJourneyDispatcherMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblJourneyDispatcherMouseClicked
        // TODO add your handling code here:
        // based on https://1bestcsharp.blogspot.com/2015/02/java-how-to-get-selected-row-values.html
        // getting model from table and selected row 
        JourneyDispatcherModelTable model = (JourneyDispatcherModelTable) tblJourneyDispatcher.getModel();
        Integer selectedRowIndex = tblJourneyDispatcher.getSelectedRow();
        // getting object based on row index
        JourneyDispatcherDto journeyDispatcherDto = model.getValue(selectedRowIndex);
        log.info("mouse clicked on journey dispatcher table at index: {}", selectedRowIndex);
        // if status is canceled or completed - update button should be hidden as the journey is already canceled or completed 
        if (journeyDispatcherDto.getJourneyStatus() == JourneyStatusDto.CANCELED
                || journeyDispatcherDto.getJourneyStatus() == JourneyStatusDto.COMPLETED) {
            this.btnUpdateJourney.setVisible(false);
            this.btnResetSelection.setVisible(true);
        } else {
            this.btnUpdateJourney.setVisible(true);
            this.btnResetSelection.setVisible(true);
        }
    }//GEN-LAST:event_tblJourneyDispatcherMouseClicked

    private void btnNewJourneyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewJourneyActionPerformed

        // reset NewJourneyForm
        newJourneyForm.display(this);
        // NewJourneyForm is shown
        newJourneyForm.setVisible(true);
        this.setVisible(false); // set JourneyDispatcherForm hidden
    }//GEN-LAST:event_btnNewJourneyActionPerformed

    private void btnResetSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetSelectionActionPerformed

        // clear table selections
        tblJourneyDispatcher.clearSelection();
        // hide update button
        this.btnUpdateJourney.setVisible(false);
        // reset journeyDispatchList to get the journey details again
        journeyDispatchList = this.dispatcherService.getDispatcherDetails();
        // reset model with journeyDispatchList
        this.tblJourneyDispatcher.setModel(new JourneyDispatcherModelTable(journeyDispatchList));
    }//GEN-LAST:event_btnResetSelectionActionPerformed

    private void btnUpdateJourneyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateJourneyActionPerformed
        // TODO add your handling code here:
        // based on https://1bestcsharp.blogspot.com/2015/02/java-how-to-get-selected-row-values.html
        // getting model from table and selected row 
        JourneyDispatcherModelTable model = (JourneyDispatcherModelTable) tblJourneyDispatcher.getModel();
        Integer selectedRowIndex = tblJourneyDispatcher.getSelectedRow();
        // getting object based on row index
        JourneyDispatcherDto journeyDispatcherDto = model.getValue(selectedRowIndex);
        // showing update journey form and sending the selected JourneyDispatcherDto object
        completeJourneyForm.display(this, journeyDispatcherDto);
        completeJourneyForm.setVisible(true);
        this.setVisible(false); // hide JourneyDispatchForm
        
    }//GEN-LAST:event_btnUpdateJourneyActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNewJourney;
    private javax.swing.JButton btnResetSelection;
    private javax.swing.JButton btnUpdateJourney;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lblGreenwichCabs;
    private javax.swing.JPanel pnlJourneyDispatcher;
    private javax.swing.JScrollPane spJourneyDispatcher;
    private javax.swing.JTable tblJourneyDispatcher;
    // End of variables declaration//GEN-END:variables
}
