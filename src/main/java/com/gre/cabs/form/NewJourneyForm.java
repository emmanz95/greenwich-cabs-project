package com.gre.cabs.form;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import com.gre.cabs.util.DefaultValueUtil;
import com.gre.cabs.util.MessageBoxUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import lombok.extern.slf4j.Slf4j;

// new journey form to create in progress journeys
// extending to JourneyDispatcherServiceForm which extends to jframe
@Slf4j
public class NewJourneyForm extends JourneyDispatcherServiceForm {
    // declare JourneyDispatcherForm variable
    private JourneyDispatcherForm dispatcherForm;
    
    /**
     * Creates new form NewBooking
     */
    public NewJourneyForm() {
        this.initComponents();
    }
    
    // display is called to reset the fields and set JourneyDispatcherForm variable
    public void display(JourneyDispatcherForm dispatcherForm) {
        this.reset(); // reset fields
        // setting the JourneyDispatcherForm
        // so we can change the visible state after new journey is saved
        this.dispatcherForm = dispatcherForm; 
    }
    
    // reset fields
    public void reset() {
        this.jCBDriverID.setSelectedIndex(0);
        this.txtDestination.setText(null);
        this.txtPickUpPoint.setText(null);
        this.jSpinnerStartTime.setValue(new Date());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        optPaneNewJourneyError = new javax.swing.JOptionPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        Date date = new Date();
        SpinnerDateModel model = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);
        jSpinnerStartTime = new JSpinner(model);
        btnSave = new javax.swing.JButton();
        lblStartTime = new javax.swing.JLabel();
        lblPickUpPoint = new javax.swing.JLabel();
        lblTheDestination = new javax.swing.JLabel();
        lblDriverId = new javax.swing.JLabel();
        jCBDriverID = new javax.swing.JComboBox<>();
        txtDestination = new javax.swing.JTextField();
        txtPickUpPoint = new javax.swing.JTextField();

        // based on https://stackoverflow.com/questions/9093448/how-to-capture-a-jframes-close-button-click-event
        // window closing button event
        // based on https://stackoverflow.com/questions/9093448/how-to-capture-a-jframes-close-button-click-event
        // window closing button event
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                log.info("closing new form");
                reset(); // reset fields
                dispatcherForm.setVisible(true);
                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
            }
        });

        jPanel1.setBackground(new java.awt.Color(77, 19, 209));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 255, 127));
        jLabel1.setText("New Booking");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(200, 247, 197));

        jSpinnerStartTime.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        JSpinner.DateEditor de = new JSpinner.DateEditor(jSpinnerStartTime, "HH:mm:ss");
        jSpinnerStartTime.setEditor(de);

        btnSave.setBackground(new java.awt.Color(136, 238, 231));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-save-4-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        lblStartTime.setText("Start time");

        lblPickUpPoint.setText("Pick-up Point");

        lblTheDestination.setText("The destination");

        lblDriverId.setText("Driver's ID");

        List<Long> values = this.dispatcherService.getDriverIds();
        Long[] array = values.toArray(new Long[values.size()]);
        jCBDriverID.setModel(new javax.swing.DefaultComboBoxModel<Long>(array));
        jCBDriverID.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        txtDestination.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        txtPickUpPoint.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblDriverId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPickUpPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTheDestination, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(119, 145, Short.MAX_VALUE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addComponent(jCBDriverID, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDestination, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPickUpPoint, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSpinnerStartTime, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStartTime)
                    .addComponent(jSpinnerStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPickUpPoint)
                    .addComponent(txtPickUpPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTheDestination)
                    .addComponent(txtDestination, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDriverId)
                    .addComponent(jCBDriverID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        // validate all the fields - i.e. data type of each field
        // not null, not empty, telephone validation etc
        // TODO check date time fields and drivers
        try {
            // save new journey
            JourneyDispatcherDto newJourneyDispatcherDto = this.saveNewJourney();
            this.reset(); // reset fields
            this.dispatcherForm.setVisible(true); // show JourneyDispatcherForm
            this.setVisible(false); // hide NewJourneyForm after saving journey detail
            // calling addNewJourneyToTable method of JourneyDispatcherForm to update the journey update table to show the changes 
            this.dispatcherForm.addNewJourneyToTable(newJourneyDispatcherDto);
            this.dispose();
        } catch (Exception ex) {
            // handle exception to extract messages
            String error = this.handleSaveException(ex);
            
            // set error messages if failed to save new journey details
            this.optPaneNewJourneyError.showMessageDialog(
                    null,
                    MessageBoxUtil.prepareScrollPane(error),
                    "New Journey Errors",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    // save new journey method
    public JourneyDispatcherDto saveNewJourney() throws DataValidationException {
        // populate all form dto values
        // TODO Data Not Found validation
        // save new journey
        JourneyDispatcherDto journeyDispatcherDto = this.dispatcherService.newJourney(
                this.convertToJourneyFormDto()
        );
        
        return journeyDispatcherDto;       
    }
    
    // get all the fields and convert to NewJourneyFormDto class
    private NewJourneyFormDto convertToJourneyFormDto() {
        final NewJourneyFormDto formDto = new NewJourneyFormDto();
        formDto.setStartTime(jSpinnerStartTime.getValue());
        formDto.setDriverId(jCBDriverID.getItemAt(jCBDriverID.getSelectedIndex()).longValue());
        formDto.setDestination(DefaultValueUtil.getValueEmptyOrNull(this.txtDestination.getText()));
        formDto.setPickUpPoint(DefaultValueUtil.getValueEmptyOrNull(this.txtPickUpPoint.getText()));
        return formDto;
    }
    
    // handling saving exception
    private String handleSaveException(Exception ex) {
        String error;
        if (ex instanceof DataValidationException) { // custom validation exception
            // casting DataValidationException and get errors
            List<String> errors = ((DataValidationException) ex).getErrors();
            // joining each error with comma separated
            error = String.join(", \n", errors);
            log.info("validation errors: {}", error);
        } else if (ex instanceof UnsupportedOperationException) {
            error = ex.getMessage();
        } else if (ex instanceof DataNotFoundException) {
            error = ex.getMessage();
        } else { // Unexpected exception
            // send friendly message for unexpected i.e. exception thrown during saving to database
            error = "New Journey Details could not be processed.";
            log.info("Unexpected errors: {}", error);
        }
        log.info("Exception message: {}", ex.getMessage());
        log.error("error", ex);
        
        return error;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnSave;
    public javax.swing.JComboBox<Long> jCBDriverID;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JSpinner jSpinnerStartTime;
    public javax.swing.JLabel lblDriverId;
    public javax.swing.JLabel lblPickUpPoint;
    public javax.swing.JLabel lblStartTime;
    public javax.swing.JLabel lblTheDestination;
    private javax.swing.JOptionPane optPaneNewJourneyError;
    public javax.swing.JTextField txtDestination;
    public javax.swing.JTextField txtPickUpPoint;
    // End of variables declaration//GEN-END:variables
}
