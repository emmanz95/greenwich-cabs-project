package com.gre.cabs.form;

import com.gre.cabs.service.JourneyDispatcherDerbyServiceImpl;
import com.gre.cabs.service.JourneyDispatcherService;

// created a parent class which will extend to inherit JFrame and access the JourneyDispatcherService object
public class JourneyDispatcherServiceForm extends javax.swing.JFrame {

    // polymorphism concept
    // instance of the service implementation class as an instance of an interface
    // refer to https://stackify.com/oop-concept-polymorphism/
    /**
     * this is based on http://tutorials.jenkov.com/java/interfaces.html
     * creating interface which will be implemented by csv, derby-db and mock
     * service classes implemented the interface to three service implementation
     * class.
     */
    // derby-db service implementation
    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherDerbyServiceImpl();
    // csv service implementation
//    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherCsvServiceImpl();
    // mock service implementation
//    protected final JourneyDispatcherService dispatcherService = new JourneyDispatcherMockServiceImpl();
}
