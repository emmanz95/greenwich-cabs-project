package com.gre.cabs.form;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Emmanuel
 */
// main menu
@Slf4j
public class GreenwichCabMenuForm extends javax.swing.JFrame {

    // instanting 2 forms
    private final JourneyDispatcherForm journeyForm = new JourneyDispatcherForm();
    private final DayJourneyReportForm dayJourneyForm = new DayJourneyReportForm();
    /**
     * Creates new form Menu
     */
    public GreenwichCabMenuForm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblMenu = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        bookingBtn = new javax.swing.JButton();
        btnDayReport = new javax.swing.JButton();

        // based on https://stackoverflow.com/questions/9093448/how-to-capture-a-jframes-close-button-click-event
        // window closing button event
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                log.info("closing menu form");
                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            }
        });

        jPanel1.setBackground(new java.awt.Color(77, 19, 209));

        lblMenu.setText("Menu");
        lblMenu.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        lblMenu.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        lblMenu.setForeground(new java.awt.Color(153, 186, 47));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(177, 177, 177)
                .addComponent(lblMenu)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMenu)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(200, 247, 197));

        bookingBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-door-8-32.png"))); // NOI18N
        bookingBtn.setText("Journey Dispatcher");
        bookingBtn.setActionCommand("bookingBtn");
        bookingBtn.setBackground(new java.awt.Color(136, 238, 231));
        bookingBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        bookingBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookingBtnActionPerformed(evt);
            }
        });

        btnDayReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconmonstr-door-8-32.png"))); // NOI18N
        btnDayReport.setText("Day Report");
        btnDayReport.setBackground(new java.awt.Color(136, 238, 231));
        btnDayReport.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnDayReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDayReportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(76, Short.MAX_VALUE)
                .addComponent(bookingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(btnDayReport, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(76, 76, 76))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDayReport, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bookingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bookingBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookingBtnActionPerformed
        // TODO add your handling code here:
        log.info("Calling booking form");
        // directing to journey dispatcher form
        journeyForm.reset(); // reseting all the fields on JourneyDispatcherForm
        journeyForm.setVisible(true); // showing the visible state of the JourneyDispatcherForm
    }//GEN-LAST:event_bookingBtnActionPerformed

    private void btnDayReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDayReportActionPerformed
        // TODO add your handling code here:
        log.info("Calling Day Report form");
        // directing to day report form
        dayJourneyForm.resetDayReportDetail(); // reseting all the fields on DayJourneyReportForm
        dayJourneyForm.setVisible(true); // showing the visible state of the v
    }//GEN-LAST:event_btnDayReportActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bookingBtn;
    private javax.swing.JButton btnDayReport;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblMenu;
    // End of variables declaration//GEN-END:variables
}
