package com.gre.cabs.form;

import com.github.lgooddatepicker.components.DatePickerSettings;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.form.table.model.DriverTakingModelTable;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

// report form to show details of the driver takings and total summary
// extending to JourneyDispatcherServiceForm which extends to jframe
@Slf4j
public class DayJourneyReportForm extends JourneyDispatcherServiceForm {
    // based on https://javarevisited.blogspot.com/2012/03/how-to-format-decimal-number-in-java.html
    // format to currency nearest 2 decimal places
    private final DecimalFormat moneyFormat = new DecimalFormat("£0.00");

    public DayJourneyReportForm() {
        initComponents();
        // reset fields
        this.resetDayReportDetail();
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        optPaneDayReportError = new javax.swing.JOptionPane();
        jPanel1 = new javax.swing.JPanel();
        lblDayReport = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblChooseDate = new javax.swing.JLabel();
        dayDatePicker = new com.github.lgooddatepicker.components.DatePicker();
        pnlReport = new java.awt.Panel();
        lblTotalForTheDay = new java.awt.Label();
        lblTotalForTheDayValue = new java.awt.Label();
        lblTotalJobsOfTheDay = new java.awt.Label();
        lblTotalJobsOfTheDayValue = new java.awt.Label();
        lblExpectedPercentage = new java.awt.Label();
        lblExpectedPercentageValue = new java.awt.Label();
        spTblDayReport = new javax.swing.JScrollPane();
        tblDayReport = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(77, 19, 209));

        lblDayReport.setText("Day Report");
        lblDayReport.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        lblDayReport.setForeground(new java.awt.Color(0, 212, 113));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDayReport)
                .addGap(171, 171, 171))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblDayReport)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(200, 247, 197));

        lblChooseDate.setText("Choose Date:");
        lblChooseDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        // based on https://github.com/LGoodDatePicker/LGoodDatePicker/issues/61
        // setting to disable editing date picker
        DatePickerSettings dps = new DatePickerSettings();
        dps.setAllowKeyboardEditing(false);
        dayDatePicker.setSettings(dps);
        dayDatePicker.setBackground(new java.awt.Color(197, 240, 154));
        dayDatePicker.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dayDatePicker.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dayDatePickerPropertyChange(evt);
            }
        });

        lblTotalForTheDay.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblTotalForTheDay.setText("Total for the Day");

        lblTotalForTheDayValue.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblTotalForTheDayValue.setText("#value");

        lblTotalJobsOfTheDay.setText("Total jobs of the day");

        lblTotalJobsOfTheDayValue.setText("#value");

        lblExpectedPercentage.setText("Expected % for the day");

        lblExpectedPercentageValue.setText("#value");

        // added our custom model and setting the initial list of driver details
        tblDayReport.setModel(new DriverTakingModelTable(new ArrayList()));
        spTblDayReport.setViewportView(tblDayReport);

        javax.swing.GroupLayout pnlReportLayout = new javax.swing.GroupLayout(pnlReport);
        pnlReport.setLayout(pnlReportLayout);
        pnlReportLayout.setHorizontalGroup(
            pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spTblDayReport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlReportLayout.createSequentialGroup()
                        .addComponent(lblTotalJobsOfTheDay, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalJobsOfTheDayValue, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlReportLayout.createSequentialGroup()
                        .addComponent(lblExpectedPercentage, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExpectedPercentageValue, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlReportLayout.createSequentialGroup()
                        .addComponent(lblTotalForTheDay, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalForTheDayValue, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        pnlReportLayout.setVerticalGroup(
            pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReportLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTotalForTheDay, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotalForTheDayValue, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalJobsOfTheDayValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotalJobsOfTheDay, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblExpectedPercentage, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblExpectedPercentageValue, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addComponent(spTblDayReport, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        lblTotalJobsOfTheDayValue.getAccessibleContext().setAccessibleName("lblTotalJobsOfTheDayValue");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlReport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(lblChooseDate, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dayDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 36, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChooseDate, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dayDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(pnlReport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dayDatePickerPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dayDatePickerPropertyChange

        // when date is chosen the property name is set as "date"
        if (evt.getPropertyName().equals("date")) {
            if (evt.getNewValue() != null) { // if date was selected
                // convert the date selected from picker
                Date date = Date.from(((LocalDate) evt.getNewValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
                // get the details of report
                DayReportDto reportDto = this.dispatcherService.getDayReport(date);
                // setting the details to the form 
                this.setDayReportDetail(reportDto);
                
            } else {                
                log.info("cleared");
                // reset fields
                this.resetDayReportDetail();
                tblDayReport.setModel(new DriverTakingModelTable(new ArrayList()));
            }
        }
    }//GEN-LAST:event_dayDatePickerPropertyChange

    // setting values from reportDto to table and labels
    private void setDayReportDetail(DayReportDto reportDto) {
        lblExpectedPercentageValue.setText(moneyFormat.format(reportDto.getExpectedPercentage() != null ? reportDto.getExpectedPercentage() : 0.00F));
        lblTotalForTheDayValue.setText(moneyFormat.format(reportDto.getTotal()!= null ? reportDto.getTotal() : 0.00F));
        lblTotalJobsOfTheDayValue.setText(reportDto.getTotalJobs() != null ? reportDto.getTotalJobs().toString() : "0");
        // set table values
        this.tblDayReport.setModel(new DriverTakingModelTable(reportDto.getDriverTakingList()));
    }
    
    // reset fields
    public void resetDayReportDetail() {
        lblExpectedPercentageValue.setText(moneyFormat.format(0.00F));
        lblTotalForTheDayValue.setText(moneyFormat.format(0.00F));
        lblTotalJobsOfTheDayValue.setText("0");        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.github.lgooddatepicker.components.DatePicker dayDatePicker;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblChooseDate;
    private javax.swing.JLabel lblDayReport;
    private java.awt.Label lblExpectedPercentage;
    private java.awt.Label lblExpectedPercentageValue;
    private java.awt.Label lblTotalForTheDay;
    private java.awt.Label lblTotalForTheDayValue;
    private java.awt.Label lblTotalJobsOfTheDay;
    private java.awt.Label lblTotalJobsOfTheDayValue;
    private javax.swing.JOptionPane optPaneDayReportError;
    private java.awt.Panel pnlReport;
    private javax.swing.JScrollPane spTblDayReport;
    private javax.swing.JTable tblDayReport;
    // End of variables declaration//GEN-END:variables
}
