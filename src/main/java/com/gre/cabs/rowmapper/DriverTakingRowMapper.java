package com.gre.cabs.rowmapper;

import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.DriverTakingDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
// getting rowmapper to set details from result set to the fields
public class DriverTakingRowMapper implements RowMapper<DriverTakingDto>  {

    @Override
    public DriverTakingDto mapRow(ResultSet rs, int i) throws SQLException {
        DriverTakingDto driverTakingDto = new DriverTakingDto();
        Float total = (rs.getFloat("avg_fare") + rs.getFloat("avg_tip"));
        driverTakingDto.setTakings(total);
        driverTakingDto.setJobsDone(rs.getLong("total_completed"));
        driverTakingDto.setPercentage((total * 0.2F));

        return driverTakingDto;
    }
    
}
