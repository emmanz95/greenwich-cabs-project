package com.gre.cabs.rowmapper;

import com.gre.cabs.dto.DriverAvailabilityDto;
import com.gre.cabs.dto.JourneyStatusDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
// getting rowmapper to set details from result set to the fields
public class DriverAvailabilityRowMapper implements RowMapper<DriverAvailabilityDto>  {

    @Override
    public DriverAvailabilityDto mapRow(ResultSet rs, int i) throws SQLException {
        DriverAvailabilityDto driverAvailabilityDto = new DriverAvailabilityDto();
        driverAvailabilityDto.setJourneyId(rs.getLong("journey_id"));
        driverAvailabilityDto.setDriverId(rs.getLong("driver_id"));
        driverAvailabilityDto.setJourneyStatus(JourneyStatusDto.valueOf(rs.getString("journey_status")));
        driverAvailabilityDto.setEndTime(rs.getObject("end_date_time") != null ? rs.getTimestamp("end_date_time") : null);
        return driverAvailabilityDto;
    }
}
