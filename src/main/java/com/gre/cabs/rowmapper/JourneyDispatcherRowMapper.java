package com.gre.cabs.rowmapper;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
// getting rowmapper to set details from result set to the fields
public class JourneyDispatcherRowMapper implements RowMapper<JourneyDispatcherDto>  {

    @Override
    public JourneyDispatcherDto mapRow(ResultSet rs, int i) throws SQLException {
        JourneyDispatcherDto dispatcherDto = new JourneyDispatcherDto();
        dispatcherDto.setJourneyId(rs.getLong("journey_id"));
        dispatcherDto.setStartTime(rs.getTimestamp("start_time"));
        dispatcherDto.setPickUpPoint(rs.getString("pickup_point"));
        dispatcherDto.setDestination(rs.getString("destination"));
        dispatcherDto.setDriverId(rs.getLong("driver_id"));
        dispatcherDto.setJourneyStatus(JourneyStatusDto.valueOf(rs.getString("journey_status")));
        dispatcherDto.setNameOfPassenger(rs.getObject("name_of_passenger") != null ? rs.getObject("name_of_passenger", String.class) : null);
        dispatcherDto.setFare(rs.getObject("fare") != null ? rs.getObject("fare", Float.class) : null);
        dispatcherDto.setOptionalTip(rs.getObject("optional_tip") != null ? rs.getObject("optional_tip", Float.class) : null);
        dispatcherDto.setTelephoneNo(rs.getObject("telephone_no") != null ? rs.getObject("telephone_no", String.class) : null);
        dispatcherDto.setEndTime(rs.getObject("end_time") != null ? rs.getTimestamp("end_time") : null);
        dispatcherDto.setActive(rs.getBoolean("active"));
        dispatcherDto.setAccount(rs.getString("account_name"));
        return dispatcherDto;
    }
}
