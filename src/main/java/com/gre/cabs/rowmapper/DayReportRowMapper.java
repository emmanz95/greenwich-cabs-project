package com.gre.cabs.rowmapper;

import com.gre.cabs.dto.form.DayReportDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
// getting rowmapper to set details from result set to the fields
public class DayReportRowMapper implements RowMapper<DayReportDto>  {

    @Override
    public DayReportDto mapRow(ResultSet rs, int i) throws SQLException {
        DayReportDto dayReportDto = new DayReportDto();
        Float total = (rs.getFloat("avg_fare") + rs.getFloat("avg_tip"));
        dayReportDto.setTotal(total);
        dayReportDto.setTotalJobs(rs.getLong("total_completed"));
        dayReportDto.setExpectedPercentage((total * 0.2F));
        
        return dayReportDto;
    }
    
}
