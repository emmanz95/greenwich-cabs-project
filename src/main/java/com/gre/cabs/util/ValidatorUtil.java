package com.gre.cabs.util;

import java.util.Date;
import java.util.Optional;

//TODO improvements on validator
//TODO maybe (OUT OF SCOPE): use spring validation via @ ANNOTATIONS instead of using util class 
// validator utility that check if values are null, empty, match cases and data type validations
public class ValidatorUtil {
    public static final String PHONE_REGEX = "^(?:0|\\+?44)(?:\\d\\s?){9,10}$";
    public static final String POSTCODE_REGEX = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";

    // returns false if string value is null or empty
    public static Boolean notNullOrEmpty(String value) {
        if (value == null || (value != null && value.trim().isEmpty())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    // returns false if Long value is null
    public static Boolean notNullOrEmpty(Long value) {
        if (value == null) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    // give string value and regex match to check for phone, postcode etc
    // returns false if string value does not match regex
    public static Boolean match(String value, String regex) {
        if (value == null || (value != null && !value.matches(regex))) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    // Optional based on https://www.baeldung.com/java-optional holds non null values
    // this can be used to check if optional value is not empty
    // Float type conversion from string value
    // exception is handled if conversion fails
    public static Optional<Float> floatDataType(String value) throws IllegalArgumentException {
        try {
            return Optional.ofNullable(Float.parseFloat(value));
        } catch (Exception ex) {
        }
        
        return Optional.empty();
    }

    // Optional based on https://www.baeldung.com/java-optional holds non null values
    // this can be used to check if optional value is not empty
    // Date type conversion from object value
    // exception is handled if conversion fails
    public static Optional<Date> dateDataType(Object value) throws IllegalArgumentException {
        try {
            // since value is of object type
            // cast the object to date type
            return Optional.ofNullable((Date) value);
        } catch (Exception ex) {
        }
        
        return Optional.empty();
    }    
}
