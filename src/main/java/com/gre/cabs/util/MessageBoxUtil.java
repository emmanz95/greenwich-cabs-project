package com.gre.cabs.util;

import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

// message box utility which sets message to a text area which has a scroll pane
// if text is long the scrollpane helps to scroll through the text
public class MessageBoxUtil {

    // based on https://stackoverflow.com/questions/26420428/how-to-word-wrap-text-in-jlabel
    // error message display with scroll pane
    public static JScrollPane prepareScrollPane(String error) {
        JTextArea textArea = new JTextArea();
        textArea.setText(error);
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        textArea.setFocusable(false);
        textArea.setBackground(UIManager.getColor("Label.background"));
        textArea.setFont(UIManager.getFont("Label.font"));
        textArea.setBorder(UIManager.getBorder("Label.border"));
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(100, 100));
        return scrollPane;
    }
}
