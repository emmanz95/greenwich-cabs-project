package com.gre.cabs.util;

// default value utility returns default value
public class DefaultValueUtil {
    // returns null if string value is empty string or null
    public static String getValueEmptyOrNull(String value) {
        return (value == null
                || (value != null && value.trim().isEmpty()))
                ? null
                : value;
    }

    // returns 0 if float value is null
    public static Float getValueOrDefault(Float value) {
        return value != null ? value : 0.00F;
    }
}
