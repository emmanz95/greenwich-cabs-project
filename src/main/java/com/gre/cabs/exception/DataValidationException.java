package com.gre.cabs.exception;

import java.util.List;

/**
 *
 * @author Emmanuel
 */
// based on https://www.baeldung.com/java-new-custom-exception
// created custom exception which specifies the validation
public class DataValidationException extends Exception {
    private List<String> errors; // stores list of errors
    public DataValidationException(List<String> errors) {
        super("Data Validation Exception"); // setting default message to parent class Exception
        this.errors = errors;
    }
    
        public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
