package com.gre.cabs.exception;

// based on https://www.baeldung.com/java-new-custom-exception
// created custom exception which specifies that data is not found
public class DataNotFoundException extends RuntimeException {
    private String error;
    public DataNotFoundException(String error) {
        super(error);
        this.error = error;
    }
}
