package com.gre.cabs.dao;

import com.gre.cabs.dto.DriverAvailabilityDto;
import com.gre.cabs.dto.JourneyDispatcherDto;
import java.util.Date;
import java.util.List;
import java.util.Optional;

// created interface method
public interface JourneyDispatcherDao {
    Long newJourney(JourneyDispatcherDto journeyDto);
    void completeJourney(JourneyDispatcherDto journeyDto);
    void cancelJourney(JourneyDispatcherDto journeyDto);
    List<JourneyDispatcherDto> getJourneyDispatcherList();
    Optional<JourneyDispatcherDto> getJourneyDispatcher(Long journeyId);
    Optional<DriverAvailabilityDto> getDriverAvailability(Long driverId, Date startTime, String journeyStatus);
}
