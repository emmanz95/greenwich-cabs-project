package com.gre.cabs.dao;

import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

// super class which will set NamedParameterJdbcTemplate and connections for derby db
@Slf4j
public class GreenwichCabsDao {
    // setting db properties
    private static final String DRIVER_CLASS_NAME = "org.apache.derby.jdbc.AutoloadedDriver";
    private static final String DATASOURCE_URL = "jdbc:derby:greenwich_cabs_db;create=true";
    private static final String USERNAME = "emmanuel";
    private static final String PASSWORD = "password";
    
    // based on https://www.baeldung.com/spring-boot-configure-data-source-programmatic
    // creating datasource with db properties
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(DRIVER_CLASS_NAME);
        dataSourceBuilder.url(DATASOURCE_URL);
        dataSourceBuilder.username(USERNAME);
        dataSourceBuilder.password(PASSWORD);
        return dataSourceBuilder.build();
    }

    // returning jdbc template to query
    public NamedParameterJdbcTemplate getNamedJdbcTemplate() {
        // giving datasource
        return new NamedParameterJdbcTemplate(getDataSource());
    }
}
