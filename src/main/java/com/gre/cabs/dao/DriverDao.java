package com.gre.cabs.dao;

import java.util.List;
import java.util.Optional;

// created interface method
public interface DriverDao {
    List<Long> getDriverIds();
    Optional<Long> findByDriverId(Long driverId);
}
