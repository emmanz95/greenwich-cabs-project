package com.gre.cabs.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
@Slf4j
public class DriverDaoImpl extends GreenwichCabsDao implements DriverDao {
    // namedJdbcTemplate to query
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    
    public DriverDaoImpl() {
        // setting namedJdbcTemplate from parent class with defined datasource
        namedJdbcTemplate = super.getNamedJdbcTemplate();
    }
    // getting list of driver ids
    @Override
    public List<Long> getDriverIds() {
        String sql = "SELECT driver_id FROM App.Driver";
        return this.namedJdbcTemplate.queryForList(sql, new HashMap(), Long.class);
    }

     // finding if driver record existed by driver id
    // Optional based on https://www.baeldung.com/java-optional holds non null values
    // this can be used to check if optional value is not empty
    @Override
    public Optional<Long> findByDriverId(Long driverId) {
        try {
            Map<String, Object> namedParameters = new HashMap<>();
            namedParameters.put("driverId", driverId);
            String sql = "SELECT driver_id FROM App.Driver WHERE driver_id = :driverId";
            return Optional.ofNullable(this.namedJdbcTemplate.queryForObject(sql, namedParameters, Long.class));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
    }    
}
