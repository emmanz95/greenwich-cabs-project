package com.gre.cabs.dao;

import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.DriverTakingDto;
import java.util.Date;

// created interface method
public interface DayReportDao {
    DriverTakingDto getDriverTaking(Date date, Long driverId);
    DayReportDto getDayReport(Date date);
}
