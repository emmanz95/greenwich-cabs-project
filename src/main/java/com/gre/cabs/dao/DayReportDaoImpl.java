package com.gre.cabs.dao;

import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.DriverTakingDto;
import com.gre.cabs.rowmapper.DayReportRowMapper;
import com.gre.cabs.rowmapper.DriverTakingRowMapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

// based on https://dzone.com/articles/how-to-specify-named-parameters-using-the-namedpar
public class DayReportDaoImpl extends GreenwichCabsDao implements DayReportDao {
    // namedJdbcTemplate to query
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    // based on https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
    // had to create the pattern that format needs to be converted to
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    
    
    public DayReportDaoImpl() {
        // setting namedJdbcTemplate from parent class with defined datasource
        namedJdbcTemplate = super.getNamedJdbcTemplate();
    }
    
    // getting overall total, expected percentage and total completed
    @Override
    public DayReportDto getDayReport(Date date) {
        try {
            Map<String, Object> namedParameters = new HashMap<>();
            namedParameters.put("date", FORMAT.format(date));
            namedParameters.put("journeyStatus", JourneyStatusDto.COMPLETED.toString());

            // using coalesce to set null fields as 0 
            final String sql = "SELECT COALESCE(SUM(j.fare), 0) as avg_fare, COALESCE(SUM(j.optional_tip), 0) as avg_tip, COUNT(*) as total_completed  FROM App.Journey j where DATE(j.start_time)=:date "
                    + "AND j.journey_status=:journeyStatus";

            // using rowmapper to set the details to get as DayReportDto object
            DayReportDto dayReportDto = this.namedJdbcTemplate.queryForObject(sql, namedParameters, new DayReportRowMapper());
            return dayReportDto;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    // getting per driver total, expected percentage and total completed
    @Override
    public DriverTakingDto getDriverTaking(Date date, Long driverId) {
        try {
            Map<String, Object> namedParameters = new HashMap<>();
            namedParameters.put("date", FORMAT.format(date));
            namedParameters.put("driverId", driverId);
            namedParameters.put("journeyStatus", JourneyStatusDto.COMPLETED.toString());

            // using coalesce to set null fields as 0 
            final String sql = "SELECT COALESCE(SUM(j.fare), 0) as avg_fare, COALESCE(SUM(j.optional_tip), 0) as avg_tip, COUNT(*) as total_completed FROM App.Journey j where DATE(j.start_time)=:date "
                    + "AND j.driver_id=:driverId AND j.journey_status=:journeyStatus";
            // using rowmapper to set the details to get as DayReportDto object
            return this.namedJdbcTemplate.queryForObject(sql, namedParameters, new DriverTakingRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
}
