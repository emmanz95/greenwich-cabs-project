package com.gre.cabs.dto;

import com.gre.cabs.dto.csvconverter.JourneyStatusEnumConverter;
import com.gre.cabs.dto.csvconverter.DateTimeConverter;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvCustomBindByPosition;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Emmanuel
 */
// journey dispatcher csv class is used for reading(converts CSV to the properties of this class) and writing to CSV
// CsvBindByPosition annotation is used to position at which column its written on CSV
/**
 * CsvCustomBindByPosition annotation is used for converting string fields from CSV to specific data type
 * CsvCustomBindByPosition annotation is only used when reading from csv and converting to data object/POJO class
 */
@Data // this uses lombok and it reduces boiler template code for getters and setters
public class JourneyDispatcherCsvDto {
    @CsvBindByPosition(position = 0)
    public Long journeyId; // datatype and field validation
    // CsvCustomBindByPosition based on https://stackoverflow.com/questions/51155224/opencsv-wrong-date-format
    // convert the string date time field on csv to date for the startTime field
    @CsvCustomBindByPosition(position = 1, converter = DateTimeConverter.class)
    @CsvBindByPosition(position = 1)
    private Date startTime;// datatype and field validation
    @CsvBindByPosition(position = 2)
    private String pickUpPoint;//field validation
    @CsvBindByPosition(position = 3)
    private String destination;//field validation
    // ENUM field which has 3 statuses (COMPLETED, CANCELLED and PROGRESS) for a journey
    // CsvCustomBindByPosition based on https://stackoverflow.com/questions/51155224/opencsv-wrong-date-format
    // convert the string field on csv to ENUM for the journeyStatus field
    @CsvCustomBindByPosition(position = 4, converter = JourneyStatusEnumConverter.class)
    @CsvBindByPosition(position = 4)
    private JourneyStatusDto journeyStatus;
    @CsvBindByPosition(position = 5)
    private Long driverId;// datatype and field validation
    @CsvBindByPosition(position = 6)
    private String nameOfPassenger;//field validation
    @CsvBindByPosition(position = 7)
    private Float fare;// datatype and field validation
    @CsvBindByPosition(position = 8)
    private String account;//field validation
    @CsvBindByPosition(position = 9)
    private Float optionalTip;// datatype and field validation
    @CsvBindByPosition(position = 10)
    private String telephoneNo;//field validation
    // CsvCustomBindByPosition based on https://stackoverflow.com/questions/51155224/opencsv-wrong-date-format
    // convert the string date time format on csv to date for the start time field
    @CsvCustomBindByPosition(position = 11, converter = DateTimeConverter.class)
    @CsvBindByPosition(position = 11)
    private Date endTime;
    @CsvBindByPosition(position = 12)
    private Boolean active;
}
