package com.gre.cabs.dto.form;

import lombok.Data;

// this class is used to get the raw fields from the UI new journey form
// the fields will be converted to journey dispatcher dto fields with specific data type
// startTime field to Date data type
@Data
public class NewJourneyFormDto {
    private Object startTime;// datatype and field validation
    private String pickUpPoint;//field validation
    private String destination;//field validation
    private Long driverId;// datatype and field validation
}
