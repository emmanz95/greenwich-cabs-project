package com.gre.cabs.dto.form;

import lombok.Data;

// this class is used for displaying each driver's takings on the DayJourneyReportForm UI
@Data
public class DriverTakingDto {
    private Long driverId;
    private Float takings; // optional tip + fare
    private Float percentage; // optional (tip + fare) * 0.2
    private Long jobsDone; // total jobs done per driver
    
    public DriverTakingDto(Long driverId, Float takings, Long jobsDone){
        this.driverId = driverId;
        this.jobsDone = jobsDone;
        this.takings = takings;
        this.percentage = takings * 0.2F;
    }
    
    public DriverTakingDto() {
    }
}
