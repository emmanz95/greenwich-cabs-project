package com.gre.cabs.dto.form;

import lombok.Data;

// this class is used to get the raw fields from the UI update journey form
// the fields will be converted to journey dispatcher dto fields with specific data type
// fare field to Float data type
// optionalTip field to Float data type
@Data
public class UpdateJourneyFormDto {
    private String fare;
    private String optionalTip;
    private String nameOfPassenger;
    private String telephoneNo;
    private String account;
}
