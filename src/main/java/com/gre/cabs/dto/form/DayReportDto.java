package com.gre.cabs.dto.form;

import java.util.Date;
import java.util.List;
import lombok.Data;

// this class is used for displaying day report on the DayJourneyReportForm UI
@Data
public class DayReportDto {
    private Float total; // total from calculating total of all driver's tip and fare
    private Long totalJobs; // total jobs done by all drivers
    private Float expectedPercentage; // expected % calculated from total * 0.2
    private List<DriverTakingDto> driverTakingList; // getting each of the driver's taking 
    private Date reportDate; // date selected to get the report

    public DayReportDto(Float total, Long totalJobs, List<DriverTakingDto> driverTakingList, Date reportDate) {
        this.total = total;
        this.totalJobs = totalJobs;
        this.expectedPercentage = total * 0.2F;
        this.driverTakingList = driverTakingList;
        this.reportDate = reportDate;
    }

    public DayReportDto() {
    }
}
