package com.gre.cabs.dto;

/**
 *
 * @author admin
 */
// Journey status is an enum which has 3 statuses
// which are completed, progress or canceled
// depending on the status of the journey we can tell if the driver is busy or not
public enum JourneyStatusDto {
    COMPLETED("completed"),
    PROGRESS("progress"),
    CANCELED("canceled");
    
    private final String statusType;
    
    public String getStatusType() {
        return statusType;
    }
    
    JourneyStatusDto(String statusType) {
        this.statusType = statusType;
    }
}
