package com.gre.cabs.dto;

import java.util.Date;
import lombok.Data;

// driver availability class with four properties
// This class is used to find the driver's availability 
// based on endtime is greater than current time and journey status 
// or completed is not in progress
@Data // this uses lombok and it reduces boiler template code for getters and setters
public class DriverAvailabilityDto {
    private Date endTime;
    private JourneyStatusDto journeyStatus;
    private Long journeyId;
    private Long driverId;
}
