package com.gre.cabs.dto.csvconverter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author admin
 */
// based on https://stackoverflow.com/questions/51155224/opencsv-wrong-date-format
// created converter which converts string field from csv to date time for a field
public class DateTimeConverter extends AbstractBeanField {
    @Override
    protected Date convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        // this is the date time format it is being stored in the CSV as string
        // based on https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
        // had to create the pattern that format needs to be converted to
        final SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy"); 
        
        // based on https://www.javatpoint.com/java-string-to-date, converted the string value to date
        Date parse;
        try {
            parse = s != null && !s.trim().isEmpty() ? format.parse(s): null;
        } catch (ParseException ex) {
            throw new RuntimeException("date parse error");
        }
        return parse;
    }
}