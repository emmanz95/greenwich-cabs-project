package com.gre.cabs.dto.csvconverter;

import com.gre.cabs.dto.JourneyStatusDto;
import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

/**
 *
 * @author admin
 */
// based on https://stackoverflow.com/questions/51155224/opencsv-wrong-date-format
// created converter which converts string field from csv to ENUM for a field
public class JourneyStatusEnumConverter extends AbstractBeanField {
    @Override
    protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        // based on https://www.java67.com/2012/10/java-enum-valueof-example-how-to-use.html
        // converted string to enum
        return JourneyStatusDto.valueOf(s);
    }
}