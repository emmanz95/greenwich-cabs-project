package com.gre.cabs.dto;

import java.util.Date;
import lombok.Data;

// Journey dispatch class is used to have all the journey dispatch details
// the details are being used when storing to database or storing in-memory using a list
/**
 * the class implements to Comparable based on https://howtodoinjava.com/sort/collections-sort/
 * this is used for sorting based on journey id in ascending order
 */

@Data // this uses lombok and it reduces boiler template code for getters and setters
public class JourneyDispatcherDto implements Comparable<JourneyDispatcherDto> {
    public Long journeyId; // datatype and field validation
    public Date startTime;// datatype and field validation
    public String pickUpPoint;//field validation
    public String destination;//field validation
    public JourneyStatusDto journeyStatus;
    public Long driverId;// datatype and field validation
    public String nameOfPassenger;//field validation
    public Float fare;// datatype and field validation
    private String account;//field validation
    public Float optionalTip;// datatype and field validation
    public String telephoneNo;//field validation
    public Date endTime;
    public Boolean active;

    @Override
    public int compareTo(JourneyDispatcherDto o) {
        return this.getJourneyId().compareTo(o.getJourneyId());
    }
}
