package com.gre.cabs.service;

import com.gre.cabs.data.JourneyData;
import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.dto.form.UpdateJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import com.gre.cabs.service.csv.CsvGenerationService;
import com.gre.cabs.service.csv.CsvGenerationServiceImpl;
import java.util.Date;
import java.util.List;
import static com.gre.cabs.data.JourneyData.generateJourneyId;

// using in-memory to save and update journey list in the JourneyDispatcherMockServiceImpl class
// using some validation methods from parent class JourneyDispatcherValidationService
public class JourneyDispatcherMockServiceImpl extends JourneyDispatcherValidationService implements JourneyDispatcherService {

    // overriding newJourney method to implement interface method
    @Override
    public JourneyDispatcherDto newJourney(NewJourneyFormDto formDto) throws DataValidationException {
        // validating new journey fields
        JourneyDispatcherDto journeyDto = super.validateNewJourney(formDto);
        // TODO check if driver id selected is available within time frame
        // checking if driver and start time selected is available 
        JourneyData.checkDriverAvailability(journeyDto.getDriverId(), journeyDto.getStartTime());
        // setting the journey to progress
        journeyDto.setJourneyStatus(JourneyStatusDto.PROGRESS);
        // generating and setting journey id
        journeyDto.setJourneyId(generateJourneyId());
        // setting active state
        journeyDto.setActive(Boolean.TRUE);
        // saving journey to the list
        Long journeyId = JourneyData.save(journeyDto);
        return journeyDto; // return the saved journey detail
    }
    
    // overriding completeJourney method to implement interface method
    @Override
    public JourneyDispatcherDto completeJourney(UpdateJourneyFormDto formDto, Long journeyId) throws DataValidationException, DataNotFoundException {
        // checking if journey data exists for journeyId
        // if journey data does not exits, it will throw custom DataNotFoundException
        JourneyDispatcherDto existJourneyDto = JourneyData.getJourneyDispather(journeyId);
        
        // checking if existing journey record is completed or cancelled and throws DataNotFoundException as
        // the record already marked completed or cancelled
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        
        // checking if the start time is greater than current time as end time should not be less than or equal to start time
        if(existJourneyDto.getStartTime().compareTo(new Date()) >= 0) {
            throw new UnsupportedOperationException("Start Time cannot be less than current Time");
        }
        
        // validating complete journey fields
        JourneyDispatcherDto journeyDto = super.validateCompleteJourney(formDto, existJourneyDto);
        // saving the updated journey record with complete status
        JourneyData.save(journeyDto);

        return journeyDto; // return the updated journey detail
    }
    
    // overriding cancelJourney method to implement interface method
    @Override
    public JourneyDispatcherDto cancelJourney(Long journeyId) throws DataValidationException, DataNotFoundException {
        // checking if journey data exists for journeyId
        // if journey data does not exits, it will throw custom DataNotFoundException
        final JourneyDispatcherDto existJourneyDto = JourneyData.getJourneyDispather(journeyId);
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        // validating cancel journey fields
        JourneyDispatcherDto journeyDto = super.validateCancelJourney(existJourneyDto);
        // saving the updated journey record with cancel status
        JourneyData.save(journeyDto);
        
        return journeyDto; // return the updated journey detail
    }
    
    // getting list of journey details
    @Override
    public List<JourneyDispatcherDto> getDispatcherDetails() {
        return JourneyData.getJourneyDetails();
    }

    // get day report based on date selection
    @Override
    public DayReportDto getDayReport(Date date) {
        return JourneyData.getDayReport(date, getDispatcherDetails());
    }

    // getting mocked driver id as list
    @Override
    public List<Long> getDriverIds() {
        return JourneyData.getDriverIds();
    }
}
