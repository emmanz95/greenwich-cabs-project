package com.gre.cabs.service;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.dto.form.UpdateJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import java.util.Date;
import java.util.List;

/**
 * this is based on http://tutorials.jenkov.com/java/interfaces.html
 * creating interface which will be implemented by csv, derby-db and mock service classes
 * implemented the interface to three service implementation class.
 * 
 * we can then create an instance of the service implementation class as an instance of an interface
 * for e.g. JourneyDispatcherService class = new JourneyDispatcherCsvServiceImpl();
 */
public interface JourneyDispatcherService {
    JourneyDispatcherDto newJourney(NewJourneyFormDto formDto) throws DataValidationException;
    JourneyDispatcherDto completeJourney(UpdateJourneyFormDto formDto, Long journeyId) throws DataValidationException, DataNotFoundException;
    JourneyDispatcherDto cancelJourney(Long journeyId) throws DataValidationException, DataNotFoundException;
    List<JourneyDispatcherDto> getDispatcherDetails();
    DayReportDto getDayReport(Date date);
    List<Long> getDriverIds();
}
