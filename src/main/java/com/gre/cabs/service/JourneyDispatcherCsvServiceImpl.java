package com.gre.cabs.service;

import com.gre.cabs.data.JourneyData;
import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.dto.form.UpdateJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import com.gre.cabs.service.csv.CsvGenerationService;
import com.gre.cabs.service.csv.CsvGenerationServiceImpl;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static com.gre.cabs.data.JourneyData.generateJourneyId;
import com.gre.cabs.dto.JourneyDispatcherCsvDto;
import java.util.stream.Collectors;

// using in-memory and csv to save and update journey list in the JourneyDispatcherMockServiceImpl class
// using some validation methods from parent class JourneyDispatcherValidationService
public class JourneyDispatcherCsvServiceImpl extends JourneyDispatcherValidationService implements JourneyDispatcherService {
    // instantiate csv generation service which will handle reading and writing to csv
    private final CsvGenerationService csvGenerationService = new CsvGenerationServiceImpl();

    // overriding newJourney method to implement interface method
    @Override
    public JourneyDispatcherDto newJourney(NewJourneyFormDto formDto) throws DataValidationException {
        // validating new journey fields
        final JourneyDispatcherDto journeyDto = super.validateNewJourney(formDto);
        // TODO check if driver id selected is available within time frame
        // checking if driver and start time selected is available 
        JourneyData.checkDriverAvailability(journeyDto.getDriverId(), journeyDto.getStartTime());
        // setting the journey to progress
        journeyDto.setJourneyStatus(JourneyStatusDto.PROGRESS);
        // generating and setting journey id
        journeyDto.setJourneyId(generateJourneyId());
        // setting active state
        journeyDto.setActive(Boolean.TRUE);
        // saving journey to csv
        this.csvGenerationService.generateCsv(Arrays.asList(convertDtoToCsv(journeyDto)));
        // saving journey to the list
        JourneyData.save(journeyDto);
        return journeyDto; // return the saved journey detail
    }

    // overriding completeJourney method to implement interface method
    @Override
    public JourneyDispatcherDto completeJourney(UpdateJourneyFormDto formDto, Long journeyId) throws DataValidationException, DataNotFoundException {
        // checking if journey data exists for journeyId
        // if journey data does not exits, it will throw custom DataNotFoundException
        JourneyDispatcherDto existJourneyDto = JourneyData.getJourneyDispather(journeyId);
        // checking if existing journey record is completed or cancelled and throws DataNotFoundException as
        // the record already marked completed or cancelled
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        // checking if the start time is greater than current time as end time should not be less than or equal to start time
        if(existJourneyDto.getStartTime().compareTo(new Date()) >= 0) {
            throw new UnsupportedOperationException("Start Time cannot be less than current Time");
        }
        // validating complete journey fields
        final JourneyDispatcherDto journeyDto = super.validateCompleteJourney(formDto, existJourneyDto);
        // saving complete journey to csv
        this.csvGenerationService.updateCsv(Arrays.asList(convertDtoToCsv(journeyDto)));
        // saving journey to the list
        JourneyData.save(journeyDto);
        return journeyDto; // return the saved journey detail
    }

    // overriding cancelJourney method to implement interface method
    @Override
    public JourneyDispatcherDto cancelJourney(Long journeyId) throws DataValidationException, DataNotFoundException {
        // checking if journey data exists for journeyId
        // if journey data does not exits, it will throw custom DataNotFoundException
        final JourneyDispatcherDto existJourneyDto = JourneyData.getJourneyDispather(journeyId);
        // checking if existing journey record is completed or cancelled and throws DataNotFoundException as
        // the record already marked completed or cancelled
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        // validating cancelled journey fields
        final JourneyDispatcherDto journeyDto = super.validateCancelJourney(existJourneyDto);
        // saving cancelled journey to csv
        this.csvGenerationService.updateCsv(Arrays.asList(convertDtoToCsv(journeyDto)));
        JourneyData.save(journeyDto); // saving journey to the list
        return journeyDto; // return the saved journey detail
    }
    
    // getting mocked driver id as list
    @Override
    public List<Long> getDriverIds() {
        return JourneyData.getDriverIds();
    }

    // the class implements to Comparable based on https://howtodoinjava.com/sort/collections-sort/
    // this is used for sorting based on journey id in ascending order
    @Override
    public List<JourneyDispatcherDto> getDispatcherDetails() {
        List<JourneyDispatcherCsvDto> csvJourneyList = this.csvGenerationService.getListFromCsv();
        // converting csv journey detail object to journey detail object
        List<JourneyDispatcherDto> convertedJourneyList = convertCsvListToDtoList(csvJourneyList);
        Collections.sort(convertedJourneyList); // sort in ascending order
        JourneyData.setJourneyDetails(convertedJourneyList);
        return JourneyData.getJourneyDetails();
    }

    // get day report based on date selection
    @Override
    public DayReportDto getDayReport(Date date) {
        return JourneyData.getDayReport(date, getDispatcherDetails());
    }
    
    // based on https://www.mkyong.com/java8/java-8-streams-map-examples/
    // converting list of JourneyDispatcherCsvDto object to list of JourneyDispatcherDto
    private List<JourneyDispatcherDto> convertCsvListToDtoList(List<JourneyDispatcherCsvDto> journeyList) {
        return journeyList.stream().map(x -> this.convertCsvToDto(x)).collect(Collectors.toList());
    }
    
    // getters and setters to convert JourneyDispatcherCsvDto to JourneyDispatcherDto
    private JourneyDispatcherDto convertCsvToDto(JourneyDispatcherCsvDto csvDto) {
        JourneyDispatcherDto dispatcherDto = new JourneyDispatcherDto();
        dispatcherDto.setAccount(csvDto.getAccount());
        dispatcherDto.setActive(csvDto.getActive());
        dispatcherDto.setDestination(csvDto.getDestination());
        dispatcherDto.setDriverId(csvDto.getDriverId());
        dispatcherDto.setEndTime(csvDto.getEndTime());
        dispatcherDto.setFare(csvDto.getFare());
        dispatcherDto.setJourneyId(csvDto.getJourneyId());
        dispatcherDto.setJourneyStatus(csvDto.getJourneyStatus());
        dispatcherDto.setNameOfPassenger(csvDto.getNameOfPassenger());
        dispatcherDto.setOptionalTip(csvDto.getOptionalTip());
        dispatcherDto.setPickUpPoint(csvDto.getPickUpPoint());
        dispatcherDto.setStartTime(csvDto.getStartTime());
        dispatcherDto.setTelephoneNo(csvDto.getTelephoneNo());
        return dispatcherDto;
    }
    
    // getters and setters to convert JourneyDispatcherDto to JourneyDispatcherCsvDto
    private JourneyDispatcherCsvDto convertDtoToCsv(JourneyDispatcherDto csvDto) {
        JourneyDispatcherCsvDto dispatcherDto = new JourneyDispatcherCsvDto();
        dispatcherDto.setAccount(csvDto.getAccount());
        dispatcherDto.setActive(csvDto.getActive());
        dispatcherDto.setDestination(csvDto.getDestination());
        dispatcherDto.setDriverId(csvDto.getDriverId());
        dispatcherDto.setEndTime(csvDto.getEndTime());
        dispatcherDto.setFare(csvDto.getFare());
        dispatcherDto.setJourneyId(csvDto.getJourneyId());
        dispatcherDto.setJourneyStatus(csvDto.getJourneyStatus());
        dispatcherDto.setNameOfPassenger(csvDto.getNameOfPassenger());
        dispatcherDto.setOptionalTip(csvDto.getOptionalTip());
        dispatcherDto.setPickUpPoint(csvDto.getPickUpPoint());
        dispatcherDto.setStartTime(csvDto.getStartTime());
        dispatcherDto.setTelephoneNo(csvDto.getTelephoneNo());
        return dispatcherDto;
    }
    
}
