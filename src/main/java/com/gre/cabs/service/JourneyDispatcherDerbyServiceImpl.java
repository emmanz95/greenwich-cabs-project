package com.gre.cabs.service;

import com.gre.cabs.dao.DayReportDao;
import com.gre.cabs.dao.DayReportDaoImpl;
import com.gre.cabs.dao.DriverDao;
import com.gre.cabs.dao.DriverDaoImpl;
import com.gre.cabs.dao.JourneyDispatcherDao;
import com.gre.cabs.dao.JourneyDispatcherDaoImpl;
import com.gre.cabs.data.JourneyData;
import com.gre.cabs.dto.DriverAvailabilityDto;
import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.DriverTakingDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.dto.form.UpdateJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

// using derby-db to save and update journey list in the JourneyDispatcherMockServiceImpl class
// using some validation methods from parent class JourneyDispatcherValidationService
public class JourneyDispatcherDerbyServiceImpl extends JourneyDispatcherValidationService implements JourneyDispatcherService {
    // instantiate driver dao to get list of drivers
    private final DriverDao driverDao = new DriverDaoImpl();
    // instantiate journey dao to insert, update and get journeys
    private final JourneyDispatcherDao journeyDao = new JourneyDispatcherDaoImpl();
    // instantiate dayReport dao to get a report of takings and driver takings
    private final DayReportDao dayReportDao = new DayReportDaoImpl();
    // based on https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
    // had to create the pattern that format needs to be converted to
    // instantiate SimpleDateFormat to formate date in dd-MM-yyyy HH:mm:ss format
    private final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 

    // overriding newJourney method to implement interface method
    @Override
    public JourneyDispatcherDto newJourney(NewJourneyFormDto formDto) throws DataValidationException {
        // based on https://www.baeldung.com/java-optional-throw-exception
        // check if driver id exists otherwise throws DataNotFoundException
        Long driverId = driverDao.findByDriverId(formDto.getDriverId()).orElseThrow(() -> new DataNotFoundException(String.format("Driver not found for %s", formDto.getDriverId())));
        // validating new journey fields
        final JourneyDispatcherDto journeyDto = super.validateNewJourney(formDto);
        // TODO check if driver id selected is available within time frame
        // checking if driver and start time selected is available 
        this.checkDriverAvailability(journeyDto);
        // setting the journey to progress
        journeyDto.setJourneyStatus(JourneyStatusDto.PROGRESS);
        // setting active state
        journeyDto.setActive(Boolean.TRUE);
        // saving journey to derby db
        Long journeyId = journeyDao.newJourney(journeyDto);
        // setting journey id generated from autogeneration from derby db
        journeyDto.setJourneyId(journeyId);
        // saving journey to the list
        JourneyData.save(journeyDto);
        return journeyDto; // return the saved journey detail
    }
    
    // checking driver availability based on driver id and start time
    private void checkDriverAvailability(JourneyDispatcherDto journeyDto) {
        // checking if there is any progress jobs for the driver
        Optional<DriverAvailabilityDto> progressAvailability = this.journeyDao.getDriverAvailability(journeyDto.getDriverId(), journeyDto.getStartTime(), "PROGRESS");
        if (progressAvailability.isPresent()) { // throws exception as driver can start after finishing of the progress jobs
            throw new UnsupportedOperationException(String.format("Driver#%s can start after finishing Journey#%s", journeyDto.getDriverId(), progressAvailability.get().getJourneyId()));
        }
        // checking if there is any completed jobs for the driver
        Optional<DriverAvailabilityDto> completedAvailability = this.journeyDao.getDriverAvailability(journeyDto.getDriverId(), journeyDto.getStartTime(), "COMPLETED");
        
        if(completedAvailability.isPresent()) { // throws exception as driver can start after 10 mins after their recently completed journey
            throw new UnsupportedOperationException(String.format("Driver#%s can start after %s", journeyDto.getDriverId(), FORMAT.format(completedAvailability.get().getEndTime())));
        }
    }

    // overriding completeJourney method to implement interface method
    @Override
    public JourneyDispatcherDto completeJourney(UpdateJourneyFormDto formDto, Long journeyId) throws DataValidationException, DataNotFoundException {
        // based on https://www.baeldung.com/java-optional-throw-exception
        // check if driver id exists otherwise throws DataNotFoundException
        JourneyDispatcherDto existJourneyDto = journeyDao.getJourneyDispatcher(journeyId).orElseThrow(() -> new DataNotFoundException(String.format("Journey not found for #%s", journeyId)));
        // checking if existing journey record is completed or cancelled and throws DataNotFoundException as
        // the record already marked completed or cancelled
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        // checking if the start time is greater than current time as end time should not be less than or equal to start time
        if(existJourneyDto.getStartTime().compareTo(new Date()) >= 0) {
            throw new UnsupportedOperationException("Start Time cannot be less than current Time");
        }
        // validating complete journey fields
        final JourneyDispatcherDto journeyDto = super.validateCompleteJourney(formDto, existJourneyDto);
        // saving complete journey to derby db
        journeyDao.completeJourney(journeyDto);
        // saving journey to the list
        JourneyData.save(journeyDto);
        return journeyDto; // return the saved journey detail
    }

    // overriding cancelJourney method to implement interface method
    @Override
    public JourneyDispatcherDto cancelJourney(Long journeyId) throws DataValidationException, DataNotFoundException {
        // checking if journey data exists for journeyId
        // if journey data does not exits, it will throw custom DataNotFoundException
        JourneyDispatcherDto existJourneyDto = journeyDao.getJourneyDispatcher(journeyId).orElseThrow(() -> new DataNotFoundException(String.format("Journey not found for #%s", journeyId)));
        // checking if existing journey record is completed or cancelled and throws DataNotFoundException as
        // the record already marked completed or cancelled
        if (!existJourneyDto.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
            throw new DataNotFoundException(String.format("Journey status is already %s", existJourneyDto.getJourneyStatus().getStatusType()));
        }
        // validating cancelled journey fields
        final JourneyDispatcherDto journeyDto = super.validateCancelJourney(existJourneyDto);
        // saving cancelled journey to derby db
        journeyDao.cancelJourney(journeyDto);
        JourneyData.save(journeyDto); // saving journey to the list
        return journeyDto; // return the saved journey detail
    }

    // getting list of journey details
    @Override
    public List<JourneyDispatcherDto> getDispatcherDetails() {
        // setting in memory list from journey data with new journey list from database
        JourneyData.setJourneyDetails(this.journeyDao.getJourneyDispatcherList());
        return JourneyData.getJourneyDetails();
    }

    // getting day report based on selected date
    @Override
    public DayReportDto getDayReport(Date date) {
        // getting report details based on date
        DayReportDto reportDto = this.dayReportDao.getDayReport(date);
        List<Long> drivers = this.getDriverIds(); // getting all the driver ids
        
        // checking where reportDto is null or total is 0
        if(reportDto == null || (reportDto != null && reportDto.getTotal().equals(0.00F))) {
            // set with default values
            reportDto = new DayReportDto();
            reportDto.setExpectedPercentage(0.00F);
            reportDto.setTotalJobs(0L);
            reportDto.setExpectedPercentage(0.00F);
            reportDto.setReportDate(date);
            // does not query the database for driver takings as total is 0
            reportDto.setDriverTakingList(this.setDriverTakingDetails(drivers, Boolean.FALSE, date));
        } else {
            reportDto.setReportDate(date);
            // does query the database for driver takings
            reportDto.setDriverTakingList(this.setDriverTakingDetails(drivers, Boolean.TRUE, date));
        }

        return reportDto; //  return the saved journey detail
    }
    
    // set driver taking details 
    private List<DriverTakingDto> setDriverTakingDetails(List<Long> drivers, Boolean dayReportDetail, Date date) {
        // adding local list variable
        List<DriverTakingDto> driverTakingList = new ArrayList();
        for (Long driverId : drivers) {
            DriverTakingDto driverTakingDto = new DriverTakingDto();
            if (dayReportDetail) { // this queries driver takings
                driverTakingDto = this.dayReportDao.getDriverTaking(date, driverId);
                driverTakingDto.setDriverId(driverId);
            } else { // setting default values
                driverTakingDto.setDriverId(driverId);
                driverTakingDto.setJobsDone(0L);
                driverTakingDto.setPercentage(0.00F);
                driverTakingDto.setTakings(0.00F);
            }
            // adding driver taking to list
            driverTakingList.add(driverTakingDto);
        }
        
        return driverTakingList; // return list of driver taking
    }

    // getting mocked driver id as list
    @Override
    public List<Long> getDriverIds() {
        return driverDao.getDriverIds();
    }
    
}
