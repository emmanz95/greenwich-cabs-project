package com.gre.cabs.service.csv;

import com.gre.cabs.dto.JourneyDispatcherCsvDto;
import java.util.List;

// creating interface which consists of abstract methods (method without a body or implementation)
public interface CsvGenerationService {
    void generateCsv(List<JourneyDispatcherCsvDto> journeyDispatcherList);
    void updateCsv(List<JourneyDispatcherCsvDto> journeyDispatcherList);
    List<JourneyDispatcherCsvDto> getListFromCsv();
}
