package com.gre.cabs.service.csv;

import com.gre.cabs.dto.JourneyDispatcherCsvDto;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

/**
 * there is no libraries in java as per research to update the file
 * therefore there are 2 csv files.
 * 
 * in progress file csv file is used for saving new journey
 * completed or rejected csv file is used for saving updated in-progress journey 
 * 
 * csv code based on https://www.callicoder.com/java-read-write-csv-file-opencsv/
 * 
 * Based on http://tutorials.jenkov.com/java/interfaces.html
 * this class implements CsvGenerationService.
 */
@Slf4j
public class CsvGenerationServiceImpl implements CsvGenerationService {
    
    // paths for 2 csv files
    // path for in progress csv files
    public static final String IN_PROGRESS_GREENWICH_CABS_PATH = "src/main/resources/csv/in_progress_greenwich_cabs.csv";
    // path for complete or rejected csv files
    public static final String COMPLETE_OR_REJECTED_GREENWICH_CABS_PATH = "src/main/resources/csv/complete_or_rejected_greenwich_cabs.csv";
    
    // creating in progress journey csv
    @Override
    public void generateCsv(List<JourneyDispatcherCsvDto> journeyDispatcherList) {
        try {
            List<JourneyDispatcherCsvDto> journeys = getListFromCsv();
            // creating writer that appends to the files
            Writer writer = Files.newBufferedWriter(Paths.get(IN_PROGRESS_GREENWICH_CABS_PATH), StandardOpenOption.APPEND);
            StatefulBeanToCsv<JourneyDispatcherCsvDto> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();
            // writing the journey list object to csv
            // object has CsvBindByPosition annotations which will write each field in specified column position
            beanToCsv.write(journeyDispatcherList);
            writer.close(); // closing the writer
        } catch (Exception ex) {
            log.error("failed to generate CSV", ex);
        }
    }
    
    // creating in completed or rejected journey csv
    @Override
    public void updateCsv(List<JourneyDispatcherCsvDto> journeyDispatcherList) {
        try {
            // creating writer that appends to the files
            Writer writer = Files.newBufferedWriter(Paths.get(COMPLETE_OR_REJECTED_GREENWICH_CABS_PATH), StandardOpenOption.APPEND);
            StatefulBeanToCsv<JourneyDispatcherCsvDto> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();
            // writing the journey list object to csv
            // object has CsvBindByPosition annotations which will write each field in specified column position
            beanToCsv.write(journeyDispatcherList);
            writer.close(); // closing the writer
        } catch (Exception ex) {
            log.error("failed to update CSV", ex);
        }
    }
    
    // merging progress and completed csv
    @Override
    public List<JourneyDispatcherCsvDto> getListFromCsv() {
        // getting progress list
        final List<JourneyDispatcherCsvDto> progress = getListOfProgressCabs();
        // getting completed and canceled list
        final List<JourneyDispatcherCsvDto> completeOrCanceledCabs = getListOfCompleteOrCanceledCabs();
        // getting all the completed and canceled journey ids
        List<Long> ids = completeOrCanceledCabs.stream().map(JourneyDispatcherCsvDto::getJourneyId).collect(Collectors.toList());
        // removing completed and canceled journey ids from progress list
        final List<JourneyDispatcherCsvDto> removeJourneysInProgressList = progress.stream().filter(x -> !new ArrayList<>(ids).contains(x.getJourneyId())).collect(Collectors.toList());
        // merging the complete or cancelled list with progress list
        List<JourneyDispatcherCsvDto> mergeList = (removeJourneysInProgressList);
        mergeList.addAll(completeOrCanceledCabs);
        return mergeList;
    }
    
    // getting list of progress cabs
    private List<JourneyDispatcherCsvDto> getListOfProgressCabs() {
        try {
            // reader
            Reader reader = Files.newBufferedReader(Paths.get(IN_PROGRESS_GREENWICH_CABS_PATH));
            // preparing the converter from csv to journey dispatcher class
            CsvToBean<JourneyDispatcherCsvDto> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(JourneyDispatcherCsvDto.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            // returning conversion
            return csvToBean.parse();
        } catch(Exception ex) {
            log.error("failed to get list of progress cabs CSV", ex);
        }
        
        return new ArrayList<>();
    }
    
    // getting list of completed or cancelled cabs
    private List<JourneyDispatcherCsvDto> getListOfCompleteOrCanceledCabs() {
        try {
            // reader 
            Reader reader = Files.newBufferedReader(Paths.get(COMPLETE_OR_REJECTED_GREENWICH_CABS_PATH));
            // preparing the converter from csv to journey dispatcher class
            CsvToBean<JourneyDispatcherCsvDto> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(JourneyDispatcherCsvDto.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            // returning conversion
            return csvToBean.parse();
        } catch (Exception ex) {
            log.error("failed to get list of progress cabs CSV", ex);
        }

        return new ArrayList<>();
    }
}
