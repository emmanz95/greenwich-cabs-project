package com.gre.cabs.service;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.NewJourneyFormDto;
import com.gre.cabs.dto.form.UpdateJourneyFormDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.exception.DataValidationException;
import com.gre.cabs.util.ValidatorUtil;
import static com.gre.cabs.util.ValidatorUtil.floatDataType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

// created a super class
// this parent class would be only validating journey details
public class JourneyDispatcherValidationService {
    // created error message constants for validation and data type errors
    public static final String PICKUP_POINT_NOT_EMPTY = "Pickup Point field cannot be empty";
    public static final String DRIVER_ID_NOT_EMPTY = "Driver Id's field cannot be empty";
    public static final String PICKUP_POINT_POSTCODE_REGEX = "Pick Up Point postcode is invalid";
    public static final String DESTINATION_NOT_EMPTY = "Destination field cannot be empty";
    public static final String DESTINATION_POSTCODE_REGEX = "Destination postcode is invalid";
    public static final String START_TIME_DATE_DATA_TYPE = "Start Time field could not be converted to date";
    public static final String FARE_FLOAT_DATA_TYPE = "Fare field could not be converted to float";
    public static final String TIP_FLOAT_DATA_TYPE = "Optional Tip field could not be converted to float";
    public static final String FARE_NOT_EMPTY = "Fare field cannot be empty";
    public static final String NAME_OF_PASSENGER_NOT_EMPTY = "Name Of Passenger field cannot be empty";
    public static final String TELEPHONE_REGEX = "Telephone is invalid";
    public static final String DESTINATION_AND_PICKUP_POINT_IS_SAME = "Destination and Pickup point must not be the same";
    
    // validation required for new journey
    protected JourneyDispatcherDto validateNewJourney(NewJourneyFormDto formDto) throws DataValidationException {
        // validating field type such as checking if fields are not empty, null etc
        final JourneyDispatcherDto afterFieldTypeDispatcherDto = this.newJourneyFieldTypeValidation(formDto);
        // validating data type such as converting fields to date or float etc
        final JourneyDispatcherDto afterDataTypeDispatcherDto = this.newJourneyDataFieldTypeValidation(formDto, afterFieldTypeDispatcherDto);
        
        
        // checking if destination field and pickup point are same
        if (afterDataTypeDispatcherDto.getDestination().equalsIgnoreCase(afterDataTypeDispatcherDto.getPickUpPoint())) {
            // throw custom error message
            throw new DataValidationException(Arrays.asList(DESTINATION_AND_PICKUP_POINT_IS_SAME));
        }

        // returns object after passing all validation checks
        return afterDataTypeDispatcherDto;
    }
    
    // validation required for complete journey
    protected JourneyDispatcherDto validateCompleteJourney(final UpdateJourneyFormDto formDto, final JourneyDispatcherDto existJourneyDto) throws DataNotFoundException, DataValidationException {
        // validating field type such as checking if fields are not empty, null etc
        final JourneyDispatcherDto afterFieldTypeDispatcherDto = this.updateJourneyFieldTypeValidation(formDto);
        // validating data type such as converting fields to date or float etc
        final JourneyDispatcherDto afterDataTypeDispatcherDto = this.updateJourneyDataFieldTypeValidation(formDto, afterFieldTypeDispatcherDto);
        // updating journey status to completed
        afterDataTypeDispatcherDto.setJourneyStatus(JourneyStatusDto.COMPLETED);
        // adding now date to end time
        afterDataTypeDispatcherDto.setEndTime(new Date());
        // setting name of account field
        afterDataTypeDispatcherDto.setAccount(formDto.getAccount());
        // populating existing journey fields to completed journey fields
        final JourneyDispatcherDto afterPopulatingDispatcherDto = this.populateDispatcherDto(afterDataTypeDispatcherDto, existJourneyDto);
        
        // returns object after passing all validation checks
        return afterPopulatingDispatcherDto;
    }
    
    // validation required for cancel journey
    protected JourneyDispatcherDto validateCancelJourney(final JourneyDispatcherDto existJourneyDto) throws DataNotFoundException {
        // populating existing journey fields to cancelled journey fields
        final JourneyDispatcherDto afterPopulatingDispatcherDto = this.populateDispatcherDto(new JourneyDispatcherDto(), existJourneyDto);
        // updating journey status to cancelled
        afterPopulatingDispatcherDto.setJourneyStatus(JourneyStatusDto.CANCELED);
        // adding now date to end time
        afterPopulatingDispatcherDto.setEndTime(new Date());
        
        // returns object after passing all validation checks
        return afterPopulatingDispatcherDto;
    }

    // populating existing journey fields to updated journey object
    protected JourneyDispatcherDto populateDispatcherDto(final JourneyDispatcherDto updateJourneyDto, final JourneyDispatcherDto existJourneyDto) {
        updateJourneyDto.setActive(existJourneyDto.getActive());
        updateJourneyDto.setDriverId(existJourneyDto.getDriverId());
        updateJourneyDto.setDestination(existJourneyDto.getDestination());
        updateJourneyDto.setPickUpPoint(existJourneyDto.getPickUpPoint());
        updateJourneyDto.setStartTime(existJourneyDto.getStartTime());
        updateJourneyDto.setJourneyId(existJourneyDto.getJourneyId());
        return updateJourneyDto;
    }

    // validating for new journey field type
    protected JourneyDispatcherDto newJourneyFieldTypeValidation(NewJourneyFormDto formDto) throws DataValidationException {
        JourneyDispatcherDto dispatcherDto = new JourneyDispatcherDto(); // instantiating journey dispatcher dto variable
        List<String> errorValidation = new ArrayList();// instantiating error list to store error messages

        // checking if driver id is not null or empty using validator utility
        Boolean driverIdNotNullOrEmpty = ValidatorUtil.notNullOrEmpty(formDto.getDriverId());
        if (!driverIdNotNullOrEmpty) {
            errorValidation.add(DRIVER_ID_NOT_EMPTY); // adds error message
        } else {
            dispatcherDto.setDriverId(formDto.getDriverId()); // sets driver id for journey dispatcher dto variable
        }

        // checking if destination is not null or empty using validator utility
        Boolean destinationNotNullOrEmpty = ValidatorUtil.notNullOrEmpty(formDto.getDestination());
        if (!destinationNotNullOrEmpty) {
            errorValidation.add(DESTINATION_NOT_EMPTY); // adds error message
        } else {
            dispatcherDto.setDestination(formDto.getDestination()); // sets destination for journey dispatcher dto variable
        }

        // checking if destination is matches postcode regex
        Boolean destinationRegex = ValidatorUtil.match(formDto.getDestination(), ValidatorUtil.POSTCODE_REGEX);
        if (!destinationRegex) {
            errorValidation.add(DESTINATION_POSTCODE_REGEX); // adds error message
        } else {
            dispatcherDto.setDestination(formDto.getDestination()); // sets destination for journey dispatcher dto variable
        }
        
        // checking if pickup point is not null or empty using validator utility
        Boolean pickUpPointNotNullOrEmpty = ValidatorUtil.notNullOrEmpty(formDto.getPickUpPoint());
        if (!pickUpPointNotNullOrEmpty) {
            errorValidation.add(PICKUP_POINT_NOT_EMPTY); // adds error message
        } else {
            dispatcherDto.setPickUpPoint(formDto.getPickUpPoint()); // sets pickup point for journey dispatcher dto variable
        }

        // checking if pickup point is matches postcode regex
        Boolean pickupPointRegex = ValidatorUtil.match(formDto.getPickUpPoint(), ValidatorUtil.POSTCODE_REGEX);
        if (!pickupPointRegex) {
            errorValidation.add(PICKUP_POINT_POSTCODE_REGEX);
        } else {
            dispatcherDto.setPickUpPoint(formDto.getPickUpPoint());// adds error message
        }

        if (errorValidation.size() > 0) { // if there are errors in the lists
            throw new DataValidationException(errorValidation); // throws custom data validation exception
        }

        return dispatcherDto; // otherwise returns the object where we setted our properties
    }

    // validating for new journey data type
    protected JourneyDispatcherDto newJourneyDataFieldTypeValidation(NewJourneyFormDto formDto, JourneyDispatcherDto dispatcherDto) throws DataValidationException {
        List<String> errorValidation = new ArrayList();// instantiating error list to store error messages

        // converting start time field to date data type
        // optional which has value or empty
        Optional<Date> dateValueType = ValidatorUtil.dateDataType(formDto.getStartTime());
        if (!dateValueType.isPresent()) { // if optional valus is empty
            errorValidation.add(START_TIME_DATE_DATA_TYPE); // adds error message for start time date data type
        } else {
            // setting start date time
            Date startDateTime = this.setCurrentDateToStartTime(dateValueType.get());
            dispatcherDto.setStartTime(startDateTime);
        }

        if (errorValidation.size() > 0) { // if there are errors in the lists
            throw new DataValidationException(errorValidation); // throws custom data validation exception
        }

        return dispatcherDto; // otherwise returns the object where we setted our properties
    }

    // validating for update journey field type
    protected JourneyDispatcherDto updateJourneyFieldTypeValidation(UpdateJourneyFormDto formDto) throws DataValidationException {
        JourneyDispatcherDto dispatcherDto = new JourneyDispatcherDto(); // instantiating journey dispatcher dto variable
        List<String> errorValidation = new ArrayList(); // instantiating error list to store error messages

        // checking if fare is not null or empty using validator utility
        Boolean fareNotNullOrEmpty = ValidatorUtil.notNullOrEmpty(formDto.getFare());
        if (!fareNotNullOrEmpty) {
            errorValidation.add(FARE_NOT_EMPTY); // adds error message
        }

        // checking if name of passenger is not null or empty using validator utility
        Boolean nameOfPassengerNotNullOrEmpty = ValidatorUtil.notNullOrEmpty(formDto.getNameOfPassenger());
        if (!nameOfPassengerNotNullOrEmpty) {
            errorValidation.add(NAME_OF_PASSENGER_NOT_EMPTY); // adds error message
        } else {
            // sets passenger name if validation passes
            dispatcherDto.setNameOfPassenger(formDto.getNameOfPassenger());
        }

        // checking if phone number matches with phone number regex using validator utility
        Boolean phoneNoRegex = formDto.getTelephoneNo() != null ? ValidatorUtil.match(formDto.getTelephoneNo(), ValidatorUtil.PHONE_REGEX) : Boolean.TRUE;
        if (!phoneNoRegex) {
            errorValidation.add(TELEPHONE_REGEX); // adds error message
        } else {
            // sets telephone if validation passes
            dispatcherDto.setTelephoneNo(formDto.getTelephoneNo());
        }

        if (errorValidation.size() > 0) { // if there are errors in the lists
            throw new DataValidationException(errorValidation); // throws custom data validation exception
        }

        return dispatcherDto; // otherwise returns the object where we setted our properties
    }

    // validating for update journey data type
    protected JourneyDispatcherDto updateJourneyDataFieldTypeValidation(UpdateJourneyFormDto formDto, JourneyDispatcherDto dispatcherDto) throws DataValidationException {
        List<String> errorValidation = new ArrayList(); // instantiating error list to store error messages

        // converting fare field to float data type
        // optional which has value or empty
        Optional<Float> fareValueType = floatDataType(formDto.getFare());
        if (!fareValueType.isPresent()) { // if optional value is empty
            errorValidation.add(FARE_FLOAT_DATA_TYPE); // adds error message
        } else {
            // setting fare field
            dispatcherDto.setFare(fareValueType.get());
        }

        if (formDto.getOptionalTip() != null) { // not require field if tip field is null
            // converting tip field to float data type
            // optional which has value or empty
            Optional<Float> tipValueType = floatDataType(formDto.getOptionalTip());
            if (!tipValueType.isPresent()) { // if optional value is empty
                errorValidation.add(TIP_FLOAT_DATA_TYPE); // adds error message
            } else {
                // setting tip field
                dispatcherDto.setOptionalTip(tipValueType.get());
            }
        }

        if (errorValidation.size() > 0) { // if there are errors in the lists
            throw new DataValidationException(errorValidation); // throws custom data validation exception
        }

        return dispatcherDto; // otherwise returns the object where we setted our properties
    }
    
    // set todays date to start time
    private Date setCurrentDateToStartTime(Date startTime) {
        Calendar calStartTime = Calendar.getInstance(); // using calendar to set start time
        Calendar calNow = Calendar.getInstance(); // using calendar to set now date
        calStartTime.setTime(startTime);
        calNow.setTime(new Date());
        // setting date to calStartTime
        calStartTime.set(Calendar.MONTH, calNow.get(Calendar.MONTH));
        calStartTime.set(Calendar.YEAR, calNow.get(Calendar.YEAR));
        calStartTime.set(Calendar.DATE, calNow.get(Calendar.DATE));
        return calStartTime.getTime(); // returning start time date
    }
}
