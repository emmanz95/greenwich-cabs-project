package com.gre.cabs.data;

import com.gre.cabs.dto.JourneyDispatcherDto;
import com.gre.cabs.dto.JourneyStatusDto;
import com.gre.cabs.dto.form.DayReportDto;
import com.gre.cabs.dto.form.DriverTakingDto;
import com.gre.cabs.exception.DataNotFoundException;
import com.gre.cabs.util.DefaultValueUtil;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.Assert;

/**
 * creating static methods and populating list to create and updates journey dispatch details
 * CSV and Mock implementation is using this static methods to query journey details
 */
@Slf4j
public class JourneyData {
    // created list to populate and update journey dispatch details
    private static List<JourneyDispatcherDto> journeyList = new ArrayList();
    // based on https://docs.oracle.com/javase/10/docs/api/java/text/SimpleDateFormat.html
    // created date time formatter to convert Date to string format
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 

    // creates new journey based on PROGRESS status
    // updates journey based on COMPLETED OR CANCELLED status
    public static Long save(JourneyDispatcherDto journeyDto) {
        switch (journeyDto.getJourneyStatus()) {
            case PROGRESS:
                journeyList.add(journeyDto);
                return journeyDto.getJourneyId();
            case COMPLETED:            
                completeJourney(journeyDto);
                return journeyDto.getJourneyId();
            case CANCELED:
                cancelJourney(journeyDto);
                return journeyDto.getJourneyId();
        }
        // throws exception if journey status does not match anything from the switch case
        throw new UnsupportedOperationException(String.format("Journey Status:%s did not match", journeyDto.getJourneyStatus()));
    }

    // find one journey dispatcher based on journey id
    public static JourneyDispatcherDto getJourneyDispather(Long journeyId) throws DataNotFoundException {
        // using java streams and filter based on https://www.mkyong.com/java8/java-8-streams-filter-examples/
        // filters based on journey id OTHERWISE throws custom exception DataNotFoundException 
        return journeyList.stream()
                .filter(x -> x.getJourneyId().compareTo(journeyId) == 0)
                .findAny()
                .orElseThrow(() -> new DataNotFoundException(String.format("Journey not found for #%s", journeyId)));        
    }
    
    // check if driver selected is available to do the job
    // driver who recently completed a job is given 10 minutes to start a new job 
    public static void checkDriverAvailability(Long driverId, Date startTime) {
        journeyList.stream()
                .filter(x -> driverAvailabilityQuery(x, driverId, startTime))
                .findAny()
                .ifPresent(x -> {
                    if (x.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)) {
                        throw new UnsupportedOperationException(String.format("Driver#%s can start after finishing Journey#%s", x.getDriverId(), x.getJourneyId()));
                    } else if (x.getJourneyStatus().equals(JourneyStatusDto.COMPLETED)) {
                        // if error throw null pointer exception for end date means that there is something WRONG with business logic
                        Date endDateTime = getEndDateTimeAfterTenMinutes(x.getEndTime());
                        throw new UnsupportedOperationException(String.format("Driver#%s can start after %s", x.getDriverId(), FORMAT.format(endDateTime)));
                    }
                });
    }
    
    // mocked driver ids
    // used for CSV and mock implementation
    public static List<Long> getDriverIds() {
        return Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L);
    }

    // get day report based on date
    // journey dispatcher list is passed to reset the values
    public static DayReportDto getDayReport(Date reportDate, List<JourneyDispatcherDto> journeys) {
        Assert.notNull(reportDate, "Report Date should not be null"); // report date parameter should not be null
        setJourneyDetails(journeys); // reset with given list of journeys
        return getCalculatedDayReport(reportDate); // give report after calculation
    }

    // get day report based on date
    private static DayReportDto getCalculatedDayReport(Date reportDate) {
        List<DriverTakingDto> driverTakingList = new ArrayList();
        // TOTAL jobs done, expected percentage  and takings by all the drivers
        Long totalJobsDone = 0L;
        Float totalDriverTakings = 0.00F;
        // calculating each driver's taking, expected percentage  and takings 
        for (Long driverId : getDriverIds()) {
            final DriverTakingDto driverTakingDto = getCalculatedDriverTaking(driverId, reportDate);
            // adding each driver taking to list
            driverTakingList.add(driverTakingDto);
            // calculate total jobs done
            totalJobsDone = totalJobsDone + driverTakingDto.getJobsDone();
            // calculate total drivers taking
            totalDriverTakings = totalDriverTakings + driverTakingDto.getTakings();
        }

        return new DayReportDto(totalDriverTakings, totalJobsDone, driverTakingList, reportDate);
    }
    
    // get each driver's taking, expected percentage  and takings 
    private static DriverTakingDto getCalculatedDriverTaking(final Long driverId, final Date reportDate) {
        // driver job done and takings
        Long driverJobsDone = 0L;
        Float driverTakings = 0.00F;
        // find completed jobs per driver
        List<JourneyDispatcherDto> completedJobsPerDrivers = getCompletedJobsPerDriver(driverId, reportDate);
        for (JourneyDispatcherDto journeyDto : completedJobsPerDrivers) {
            // calculate per driver takings
            driverTakings = driverTakings
                    + DefaultValueUtil.getValueOrDefault(journeyDto.getOptionalTip())
                    + DefaultValueUtil.getValueOrDefault(journeyDto.getFare());
            ++driverJobsDone; // calculate jobs done per driver
        }
        return  new DriverTakingDto(driverId, driverTakings, driverJobsDone);
    }

    // based on https://stackoverflow.com/questions/7609582/how-to-get-date-part-dispose-of-time-part-from-java-util-date
    // to only check if the DATE PART between start time and date selected matches
    private static List<JourneyDispatcherDto> getCompletedJobsPerDriver(Long driverId, Date date) {
        // get a list of journey list that are complete for a driver based on a selected date
        return journeyList.stream()
                .filter(x -> x.getDriverId().equals(driverId)
                && DateUtils.truncatedCompareTo(x.getStartTime(), date, Calendar.DAY_OF_MONTH) == 0
                && x.getJourneyStatus().equals(JourneyStatusDto.COMPLETED))
                .collect(Collectors.toList());
    }

    // query to find out the availability of the driver
    private static boolean driverAvailabilityQuery(JourneyDispatcherDto x, Long driverId, Date startTime) {
        // check if the driver id matches
        boolean driverCheck = x.getDriverId().compareTo(driverId) == 0;
        // any journeys that are either completed or in progress 
        boolean journeyStatusCheck = (x.getJourneyStatus().equals(JourneyStatusDto.COMPLETED) || x.getJourneyStatus().equals(JourneyStatusDto.PROGRESS)); 
        // add 10 minutes to completed job to give the driver some time to start new job
        Date endDateTime = getEndDateTimeAfterTenMinutes(x.getEndTime());
        log.info("end date time value = {}", endDateTime);

        // check if start time provided for new job doesnt start between the driver's previous job start time and end time
        boolean inProgressStartTimeCheck = (startTime.compareTo(x.getStartTime()) >= 0 && endDateTime == null);
        boolean inProgressAndCompleteStartTimeCheck = (endDateTime != null && startTime.compareTo(x.getStartTime()) >= 0 && startTime.compareTo(endDateTime) < 0);
        boolean startTimeCheck = ((inProgressStartTimeCheck || inProgressAndCompleteStartTimeCheck));

        return driverCheck && journeyStatusCheck && startTimeCheck;
    }

    // add 10 minutes to journey end time 
    public static Date getEndDateTimeAfterTenMinutes(Date journeyEndTime) {
        if (journeyEndTime != null) { // check whether journey end time is not null
            Calendar endTime = Calendar.getInstance();
            endTime.setTime(journeyEndTime);
            endTime.add(Calendar.MINUTE, 10);
            return endTime.getTime();
        }

        return null;
    }

    // find journey based on id and then mark journey record to complete and set fare,
    // telephone number, account, tip, name of passenger
    // and end time fields
    private static void completeJourney(JourneyDispatcherDto journeyDto) {
        journeyList.stream()
                .filter(x -> x.getJourneyId().compareTo(journeyDto.getJourneyId()) == 0)
                .forEach(j -> {
                    j.setFare(journeyDto.getFare());
                    j.setTelephoneNo(journeyDto.getTelephoneNo());
                    j.setAccount(journeyDto.getAccount());
                    j.setOptionalTip(journeyDto.getOptionalTip());
                    j.setJourneyStatus(JourneyStatusDto.COMPLETED);
                    j.setNameOfPassenger(journeyDto.getNameOfPassenger());
                    j.setEndTime(journeyDto.getEndTime());
                });
    }

    // find journey based on id and then mark journey record to cancelled and set end time field
    private static void cancelJourney(JourneyDispatcherDto journeyDto) {
        journeyList.stream()
                .filter(x -> x.getJourneyId().equals(journeyDto.getJourneyId()))
                .forEach(j -> {
                    j.setJourneyStatus(JourneyStatusDto.CANCELED);
                    j.setEndTime(journeyDto.getEndTime());
                });
    }

    // give all the list of journey details
    public static List<JourneyDispatcherDto> getJourneyDetails() {
        return journeyList;
    }
    
    // set journey details
    // this can be used to reset the list of journey list 
    public static void setJourneyDetails(List<JourneyDispatcherDto> journeys) {
        journeyList = journeys;
    }

    // generate unique journey id using timestamp
    // based on https://www.mkyong.com/java/how-to-get-current-timestamps-in-java/
    public static Long generateJourneyId() {
        return new Timestamp(System.currentTimeMillis()).getTime();
    }
}
