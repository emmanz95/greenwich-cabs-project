package com.gre.cabs;

import com.gre.cabs.form.GreenwichCabMenuForm;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * based on https://github.com/lucasnascimento/spring-boot-swing/blob/master/src/main/java/br/com/lnprojetos/springbootswing/Application.java
 * to create spring boot swing application
 */
@SpringBootApplication
public class Application {

    // instantiating menu form
    private final static GreenwichCabMenuForm menu = new GreenwichCabMenuForm();
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(
                Application.class).headless(false).run(args);
                menu.setVisible(true); // when application is running, set visible of menu form to true
    }
}
